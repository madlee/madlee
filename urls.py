from django.conf.urls import include, url
from django.contrib import admin

from django.views.decorators.cache import cache_page

from . import views
from regia.Ra import urls as urls_Ra

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    url(r'^Ra/', include(urls_Ra)),

    url(r'^(.+)\.html$', views.render_file, {'basic_path': '%s.html'}),
    url(r'^(.+)\.form$', views.render_file, {'basic_path': '%s.form'}),
]

