#!/usr/bin/env python
import os
import sys
from os.path import join as join_path, abspath as abs_path

if __name__ == "__main__":
    sys.path[0] = abs_path(join_path(__file__, '../..'))
    sys.path.append(abs_path(join_path(__file__, '../../../fushan/regia')))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "madlee.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
