define(function() {

  var Madlee = {}

  Madlee.angular_post = ['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.transformRequest = [function(data)
    {
      var param = function(obj)
      {
        var query = '';
        var name, value, fullSubName, subName, subValue, innerObj, i;
        
        for(name in obj)
        {
          value = obj[name];
          
          if(value instanceof Array)
          {
            for(i=0; i<value.length; ++i)
            {
              subValue = value[i];
              fullSubName = name + '[' + i + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value instanceof Object)
          {
            for(subName in value)
            {
              subValue = value[subName];
              fullSubName = name + '[' + subName + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value !== undefined && value !== null)
          {
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
          }
        }
        
        return query.length ? query.substr(0, query.length - 1) : query;
      };
      
      return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
  }]

  Madlee.angular_config = function(app) {
    app.config(this.angular_post)
    app.config(['$interpolateProvider', function($interpolateProvider) {
      $interpolateProvider.startSymbol('{[{');
      $interpolateProvider.endSymbol('}]}');
    }])

    app.directive('popup', function() {
      return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
          ngModel: '=',
          options: '=popup'
        },
        link: function(scope, element) {
          scope.$watch('ngModel', function(val) {
            element.attr('data-content', val);
          });

          var options = scope.options || {} ; 

          var title = options.title || null;
          var placement = options.placement || 'right';
          var html = options.html || false;
          var delay = options.delay ? angular.toJson(options.delay) : null;
          var trigger = options.trigger || 'hover';

          element.attr('title', title);
          element.attr('data-placement', placement);
          element.attr('data-html', html);
          element.attr('data-delay', delay);
          element.popover({ trigger: trigger });
        }
      };
    });
  }

  Madlee.getCookie = function(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = $.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  };

  Madlee.csrftoken = Madlee.getCookie('csrftoken');

  Madlee.csrfSafeMethod = function(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  Madlee.sameOrigin = function(url) {
      // test that a given url is a same-origin URL
      // url could be relative or scheme relative or absolute
      var host = document.location.host; // host + port
      var protocol = document.location.protocol;
      var sr_origin = '//' + host;
      var origin = protocol + sr_origin;
      // Allow absolute or scheme relative URLs to same origin
      return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
          (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
          // or any other URL that isn't scheme relative or absolute i.e relative.
          !(/^(\/\/|http:|https:).*/.test(url));
  }
  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!Madlee.csrfSafeMethod(settings.type) && !this.crossDomain) {
              // Send the token to same-origin, relative URLs only.
              // Send the token only if the method warrants CSRF protection
              // Using the CSRFToken value acquired earlier
              xhr.setRequestHeader("X-CSRFToken", Madlee.csrftoken);
          }
      }
  });

  Madlee.login_first = function() {
    var me = this
    if (is_undefined(me.user)) {
      $.getJSON('/madlee/user/who_am_i', function(data) {
        me.user = data
        $('#user-label').text(me.user.username)
        $('.navbar').show();

        if (me.user.is_superuser) {
          $('.ctrl-admin').show()
        }
        else {
          $('.ctrl-admin').hide()
        }
      }).error(function() {
        window.location = '#/login'
      })
    }
    else {
      $('#user-label').text(me.user.username)
      $('.navbar').show();

      if (me.user.is_superuser) {
        $('.ctrl-admin').show()
      }
      else {
        $('.ctrl-admin').hide()
      }
    }
  }

  Madlee.logout = function(href) {
    this.user = undefined;
    $('#user-label').text('')
    $('.navbar').hide();
    $.post('/madlee/user/logout')
    var url = $(href).attr('href')
    if (is_undefined(url) || !url) {
      url = "#/login"
    }

    window.location = url
  }

  Madlee.who_am_i = function() {
    var me = this
    $('.user-panel').hide()
    $.getJSON('/madlee/user/who_am_i', function(data) {
      me.user = data
      $('#user-label').text(me.user.username)
      $('.navbar').show();

      $('.user-panel-normal').show()
    }).error(function() {
      $('.user-panel-anonymous').show()
    })
  }

  Madlee.active_navbar_tab = function(tab) {
    var tab_class="ctrl-"+tab
    $('ul.navbar-nav li').each(function(i, v) {
      v = $(v)
      if (v.hasClass(tab_class)) {
        $(v).addClass('active')
      }
      else {
        $(v).removeClass('active')
      }
    })
  }

  Madlee.download_table = function(href, filename, table) {
    if (is_undefined(table)) {
      table = $(href).closest('table')
    }

    if (is_undefined(filename)) {
      filename = table.find('caption')
      if (is_undefined(filename)) {
      }
      else {

      }
    }
  }

  Madlee.from_now = function(timez) {
    return moment(timez).fromNow()
  }

  Madlee.fixed_n = function(v, n) {
      var bases = [1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000]
      v *= bases[n]
      v = Math.round(v)
      v = v.toString()
      if (v === '0') {
        return '0.'+bases[n].toString().substring(1) 
      }
      else {
        var vhead;
        if (v.length > n) {
          vhead = v.substring(0, v.length-n)
        }
        else {
          vhead = '0'
        }
        var vtail = v.substring(v.length-n)
        if (vtail.length < n) {
          var delta = n - vtail.length;
          vtail = bases[delta].toString().substring(1) + vtail;
        }

        return [vhead, vtail].join('.')
      }
  }

  Madlee.color_to_val = function(color) {
    if (color.substring(0, 1) === '#') {
      color = color.substring(1)
    }

    var result = []
    if (color.length === 3) {
      for (var i = 0; i < 3; ++i) {
        var color_i = color.substring(i, 1)
        result.push(parseInt(color_i, 16)/15.0)
      }
    }
    else if (cr.length === 6) {
      for (var i = 0; i < 3; ++i) {
        var color_i = color.substring(i, 2)
        result.push(parseInt(color_i, 16)/255.0)
      }
    }
    return result
  }

  Madlee.val_to_color = function(val) {
    var result = ['#']
    for (var i = 0; i < val.length; ++i) {
      val[i] = Math.floor(val[i]*255+.5)
      result.push(Math.floor(val[i] / 16).toString(16))
      result.push((val[i] % 16).toString(16))
    }

    return result.join('')
  }

  Madlee.colorize = function(val, cr_min, cr_max, val_min, val_max) {
    if (is_undefined(val_min)) {
      val_min = 0
    }
    if (is_undefined(val_max)) {
      val_max = 1
    }
    if (val < val_min) {
      val = val_min
    }
    else if (val > val_max) {
      val = val_max
    }

    val = (val - val_min) / (val_max-val_min)
    cr_min = Madlee.color_to_val(cr_min)
    cr_max = Madlee.color_to_val(cr_max)
    var result = []
    for (var i = 0; i < cr_min.length; ++i) {
      result.push(val*(cr_max[i]-cr_min[i]) + cr_min[i])
    }
    return Madlee.val_to_color(val)
  }

  
  Madlee.RegisterCtrl = ['$scope', '$http', function ($scope, $http) {
    $scope.accept_eula = false;
    $scope.accept_eula = function() {
      $scope.eula_accepted = true;
    }
  }]

  Madlee.required_value = function(input) {
    var txt = $.trim($(input).val());
    if (txt == "") {
      Madlee.show_error_tip(input);
      return null;
    }
    else {
      Madlee.hide_error_tip(input);
      return txt;
    }
  }

  Madlee.hide_error_tip = function (input, error) {
    if (typeof(error) == "undefined") {
      error="has-error";
    }
    input = $(input);
    input.tooltip('hide');
    var input0 = input.parent();
    while (input0 != null && !input0.hasClass("form-group")) {
      input0 = input0.parent();
    }
    if (input0 == null) {
      input.removeClass(error);
    }
    else {
      input0.removeClass(error);
    }
  }

  Madlee.show_error_tip = function(input, error) {
    if (typeof(error) == "undefined") {
      error="has-error";
    }
    input = $(input);
    input.focus();
    input.tooltip('show');
    var input0 = input.parent();
    while (input0 != null && !input0.hasClass("form-group")) {
      input0 = input0.parent();
    }
    if (input0 == null) {
      input.addClass(error);
    }
    else {
      input0.addClass(error);
    }
  }

  Madlee.highlight_text = function(text, term) {
    if (term == null) {
      return encode_html(text)
    }
    else {
      var result = []
      var i1 = 0
      while (i1 < text.length) {
        var i2 = text.indexOf(term, i1)
        if (i2 == -1) {
          result.push(encode_html(text.substring(i1)))
          break 
        }
        else {
          result.push(encode_html(text.substring(i1, i2)))
          i1 = i2+term.length
          result.push('<span class="highlight">')
          result.push(encode_html(text.substring(i2, i1)))
          result.push('</span>')
        }
      }

      return result.join("")
    }
  }

  return Madlee
})

