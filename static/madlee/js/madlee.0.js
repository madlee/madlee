function htmlEncode(value){
    if (value) {
        return jQuery('<div />').text(value).html();
    } else {
        return '';
    }
}
 
function htmlDecode(value) {
    if (value) {
        return jQuery('<div />').html(value).text();
    } else {
        return '';
    }
}

function is_undefined(v) {
  return typeof(v) === 'undefined';
}

function required_field(input) {
  var text = jQuery.trim(input.val())
  if (text === '') {
    input.closest('div').addClass("has-error")
    return null;
  }
  else {
    input.closest('div').removeClass("has-error")
    return text;
  }

}

var show_message = function(message, convertHTML, cls, target) {
  if (is_undefined(target)) {
    target = jQuery("#message-panel")
  }

  if (is_undefined(convertHTML)) {
    convertHTML = true;
  }
  if (convertHTML) {
    message = htmlEncode(message)
  }
  if (is_undefined(cls)) {
    cls = "info"
  }

  message = ['<div class="alert alert-', cls, ' alert-dismissible" role="alert">',
    '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>',
    message, 
    '</div>']

  jQuery(target).html(message.join(''))
}

var show_error = function(message, target) {
  message = '<strong>Error: </strong>' + htmlEncode(message)
  show_message(message, false, "danger", target)
}

var show_warning = function(message, target) {
  message = '<strong>Warning: </strong>' + htmlEncode(message)
  show_message(message, false, "warning", target)
}

var show_success = function(message, target) {
  message = '<strong>Success: </strong>' + htmlEncode(message)
  show_message(message, false, "success", target)
}


Date.prototype.format = function(format){ 
    var o = { 
        "M+" : this.getMonth()+1, //month 
        "d+" : this.getDate(), //day 
        "h+" : this.getHours(), //hour 
        "m+" : this.getMinutes(), //minute 
        "s+" : this.getSeconds(), //second 
        "q+" : Math.floor((this.getMonth()+3)/3), //quarter 
        "S" : this.getMilliseconds() //millisecond 
    } 

    if(/(y+)/.test(format)) { 
        format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
    } 

    for(var k in o) { 
        if(new RegExp("("+ k +")").test(format)) { 
            format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
        } 
    } 
    return format; 
}

fixed_n = function(v, n) {
    var bases = [1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000]
    v *= bases[n]
    v = Math.round(v)
    v = v.toString()
    if (v === '0') {
      return '0.'+bases[n].toString().substring(1) 
    }
    else {
      var vhead;
      if (v.length > n) {
        vhead = v.substring(0, v.length-n)
      }
      else {
        vhead = '0'
      }
      var vtail = v.substring(v.length-n)
      if (vtail.length < n) {
        var delta = n - vtail.length;
        vtail = bases[delta].toString().substring(1) + vtail;
      }

      return [vhead, vtail].join('.')
    }
}



var angular_post = function($httpProvider) {
  // setup CSRF support
  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

  // http://victorblog.com/2012/12/20/make-angularjs-http-service-behave-like-jquery-ajax/
  // Rewrite POST body data
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data)
  {
    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */ 
    var param = function(obj)
    {
      var query = '';
      var name, value, fullSubName, subName, subValue, innerObj, i;
      
      for(name in obj)
      {
        value = obj[name];
        
        if(value instanceof Array)
        {
          for(i=0; i<value.length; ++i)
          {
            subValue = value[i];
            fullSubName = name + '[' + i + ']';
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += param(innerObj) + '&';
          }
        }
        else if(value instanceof Object)
        {
          for(subName in value)
          {
            subValue = value[subName];
            fullSubName = name + '[' + subName + ']';
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += param(innerObj) + '&';
          }
        }
        else if(value !== undefined && value !== null)
        {
          query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }
      }
      
      return query.length ? query.substr(0, query.length - 1) : query;
    };
    
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
}
var collect_cell = function(cell, row_value) {
  cell = jQuery(cell)
  var text = jQuery.trim(cell.text())
  row_value.push(text)

  var colspan = cell.attr('colspan')
  if (colspan) {
    for (var j = 1; j < colspan; ++j) {
      row_value.push(null)
    }
  }
}

var collect_table = function(table) {

  var buffer = []
  table.find('tr').each(function(i, row) {
    row = jQuery(row)
    var row_value = []
    row.find('th').each(function(i, cell) {
      collect_cell(cell, row_value)
    });
    row.find('td').each(function(i, cell) {
      collect_cell(cell, row_value)
    });
    buffer.push(row_value)
  })
  return buffer;
}

var render_cell = function(cell, row_value) {
  cell = jQuery(cell)
  var text = jQuery.trim(cell.text())
  if (text === '') {
    row_value.push('')
  }
  else {
    row_value.push(text)
  }
  var colspan = cell.attr('colspan')
  if (colspan) {
    for (var j = 1; j < colspan; ++j) {
      row_value.push('')
    }
  }
}

var download_table = function(table, filename, href, cell_sep) {
  if (is_undefined(cell_sep)) {
    cell_sep = ','
  }

  var buffer = []
  table.find('tr').each(function(i, row) {
    row = jQuery(row)
    var row_value = []
    row.find('th').each(function(i, cell) {
      render_cell(cell, row_value)
    });
    row.find('td').each(function(i, cell) {
      render_cell(cell, row_value)
    });
    buffer.push(row_value.join(cell_sep))
  })
  buffer.push('')

  if (buffer.length > 0) {
    var blob = new Blob([buffer.join('\n')], {"type": "text/csv;charset=utf8;"});
    if(navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, filename);
    }
    else {
      if (typeof(href) ==='undefined') {
        href = jQuery(this)
      }
      href.attr('href', window.URL.createObjectURL(blob))
      href.attr('download', filename)
      return true;
    }
  }
  
  return false;
}

var show_ajax_error = function(jqXHR , textStatus, errorThrown, target) {
  if (!is_undefined(jqXHR.responseJSON) && !is_undefined(jqXHR.responseJSON.detail)) {
    if (jqXHR.responseJSON.detail) {
      show_error(jqXHR.responseJSON.detail, target)
      return;
    }
  }
  show_error(errorThrown, target)
}

