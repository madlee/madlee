requirejs.config({  
    paths : {  
        jquery      : 'http://cdn.staticfile.org/jquery/1.11.1/jquery.min',  
        bootstrap   : 'http://cdn.staticfile.org/twitter-bootstrap/3.2.0/js/bootstrap.min', 
        Madlee      : '/static/madlee/js/madlee', 
        domReady    : '/static/requireJS/domReady'
    }  
});  

require(['Madlee'], function(Madlee) {

	tinymol = angular.module('tinymol', 
	  ['ngRoute', 'tinymolControllers'])

	tinymol.config(['$routeProvider', function($routeProvider) {
	  $routeProvider.
	    when('/home', {templateUrl: 'home.html',   controller: 'HomeCtrl'}).
	    when('/about', {templateUrl: 'about.html',   controller: 'AboutCtrl'}).
	    when('/register', {templateUrl: 'register.html',   controller: 'RegisterCtrl'}).
	    otherwise({redirectTo: '/home'});
	}]);

	Madlee.angular_config(tinymol)

	tinymolControllers = angular.module('tinymolControllers', []);

	tinymolControllers.controller('RegisterCtrl', Madlee.RegisterCtrl)

	tinymolControllers.controller('AboutCtrl', ['$scope', '$http', function ($scope, $http) {
	 
	}]);


	tinymolControllers.controller('HomeCtrl', ['$scope', '$http', function ($scope, $http) {
	 
	}]);

	tinymolControllers.controller('UserCtrl', ['$scope', '$http', function ($scope, $http) {
	 
	}]);

	require(['domReady!'],function(document){
        angular.bootstrap(document, ['tinymol']);
        Madlee.who_am_i();
        $('#btn-login').click(function(btn) {
        	var vv = $(btn.target).closest('.modal').find('form')
        	$(vv).validator('validate')
        })
    });
})

