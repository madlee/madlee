var TME = {};

TME.fixed_int = function(v, n) {
	v = v.toString();
	n -= v.length;
	var result = [];
	for (var i = 0; i < n; ++i) {
		result.push(' ');
	}
	result.push(v)
	return result.join('');
}

TME.fixed_3 = function(v) {
	v = v.toString();
	switch (v.length) {
		case 0:
			return "   ";
		case 1:
			return "  " + v;
		case 2:
			return " " + v;
		default:
			return v;
	}
}

TME.fixed_4 = function(v) {
	v = v.toString();
	switch (v.length) {
		case 0:
			return "    ";
		case 1:
			return "   " + v;
		case 2:
			return "  " + v;
		case 3:
			return " " + v;
		default:
			return v;
	}
}

TME.has_defined = function(v) {
	return typeof(v) != "undefined"
}

TME.set_id = function(div, prefix) {
	var me = this
	if (!me.has_defined(me.latest_id)) {
		me.latest_id = 10000
	}
	var result = div.attr("id")
	if (me.has_defined(result)) {
		result = prefix + me.latest_id.toString()
		div.attr("id", result)
		me.latest_id += 1
	}
	return result
}

TME.fixed_float = function(v) {
	v = v.toFixed(4);
	var n = 10 - v.length;
	var result = []
	for (var i = 0; i < n; ++i) {
		result.push(' ');
	}
	result.push(v);
	return result.join('');
}

// Decide the intersect point of a segment and a rectangle.
// assert pt1 is in the middle of the rectangle.
// assert x1 < x2, y1 < y2
TME.__intersectRectangle = function(pt1, pt2, rect) {
	var x1 = rect.x1, x2 = rect.x2, y1 = rect.y1, y2 = rect.y2;
	var XY = TME.XY;
	var deltaPx = pt2.x-pt1.x, deltaPy = pt2.y-pt1.y;
	var deltaRx = x2-x1, deltaRy = y2-y1;
	var lamda = null, mu=null;
	if (pt2.x < x1) {
		lamda = (x1-pt1.x)/deltaPx;
		mu = ((x1-pt1.x)*deltaPy -deltaPx*(y1-pt1.y))/deltaPx/deltaRy;
		if (lamda <= 1 && 0 <= mu && mu <= 1) {
			return new XY(x1, pt1.y+lamda*deltaPy);
		}
	}
	if (pt2.x > x2) {
		lamda = (x2-pt1.x)/deltaPx;
		mu = ((x2-pt1.x)*deltaPy -deltaPx*(y1-pt1.y))/deltaPx/deltaRy;
		if (lamda <= 1 && 0 <= mu && mu <= 1) {
			return new XY(x2, pt1.y+lamda*deltaPy);
		}
	}
	if (pt2.y < y1) {
		lamda = (y1-pt1.y)/deltaPy;
		mu = (deltaPx*(y1-pt1.y) - (x1-pt1.x)*deltaPy)/ deltaRx/deltaPy;

		if (lamda <= 1 && 0 <= mu && mu <= 1) {
			return new XY(pt1.x+lamda*deltaPx, y1);
		}
	}
	if (pt2.y > y2) {
		lamda = (y2-pt1.y)/deltaPy;
		mu = (deltaPx*(y2-pt1.y) - (x1-pt1.x)*deltaPy)/ deltaRx/deltaPy;

		if (lamda <= 1 && 0 <= mu && mu <= 1) {
			return new XY(pt1.x+lamda*deltaPx, y2);
		}
	}
	return pt1;
}

	
TME.XY = function(x, y) {
	var me = this;
	me.x = x;
	me.y = y;

	me.assign = function(x, y) {
		var me = this;
		if (typeof(y) == 'undefined') {
			me.x = x.x;
			me.y = x.y;
		}
		else {
			me.x = x;
			me.y = y;
		}
	}

	me.addBy = function(v2, y) {
		var me = this;
		if (typeof(y) == 'undefined') {
			me.x += v2.x;
			me.y += v2.y;
		}
		else {
			me.x += v2;
			me.y += y;
		}
		return me;
	}
	me.add = function(v2, y) {
		var me = this;
		var result = new TME.XY(me.x, me.y);
		result.addBy(v2, y);
		return result;
	}
	
	me.subBy = function(v2, y) {
		var me = this;
		if (typeof(y) == 'undefined') {
			me.x -= v2.x;
			me.y -= v2.y;
		}
		else {
			me.x -= v2;
			me.y -= y;
		}
		return me;
	}
	me.sub = function(v2, y) {
		var me = this;
		var result = new TME.XY(me.x, me.y);
		result.subBy(v2, y);
		return result;
	}
	
	me.mulBy = function(v2, y) {
		var me = this;
		if (typeof(y) == 'undefined') {
			var xNew = me.x * v2.x - me.y * v2.y;
			var yNew = me.x * v2.y + me.y * v2.x;
			me.x = xNew;
			me.y = yNew;
		}
		else {
			var xNew = me.x * v2 - me.y * y;
			var yNew = me.x * y + me.y * v2;
			me.x = xNew;
			me.y = yNew;
		}
		return me;
	}
	me.mul = function(v2, y) {
		var me = this;
		var result = new TME.XY(me.x, me.y);
		result.mulBy(v2, y);
		return result;
	}
	
	me.scaleBy = function(v2) {
		var me = this;
		me.x *= v2;
		me.y *= v2;
		return me;
	}
	me.scale = function(v2) {
		return new TME.XY(this.x * v2, this.y * v2);
	}
	
	me.abs = function() {
		var me = this;
		return Math.sqrt(me.x*me.x + me.y*me.y);
	}

	me.toString = function() {
		return "(" + this.x + ", " + this.y + ")"; 
	}
}

TME.download = function(url, data){
    if (typeof(data) != 'string') {
    	data = jQuery.param(data);
    }
    var inputs = '';
    jQuery.each(data.split('&'), function(){ 
        var pair = this.split('=');
        inputs+='<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />'; 
    });
    jQuery('<form action="'+ url +'" method="post">'+inputs+'</form>')
    .appendTo('body').submit().remove();
};


TME.ELEMENTS = ("Q H He Li Be B C N O F Ne Na Mg Al Si P S Cl Ar"
	+ " K Ca Sc Ti V Cr Mn Fe Co Ni Cu Zn Ga Ge As Se Br Kr"
	+ " Rb Sr Y Zr Nb Mo Tc Ru Ph Pd Ag Cd In Sn Sb Te I Xe"
	+ " Cs Ba La Ce Pr Nd Pm Sm Eu Gd Tb Dy Ho Er Tm Yb Lu Hf Ta"
	+ " W Re Os Ir Pt Au Hg Tl Pb Bi Po At Rn"
	+ " Fr Ra Ac Th Pa U Np Pu Am Cm Bk Cf Es Fm Md No Lr").split();

TME.ATOM_COLORS={"H":"#34B4CC", "B":"#BF5600", "Si":"#FEC451", "Ge":"#BF0800", "N":"#0000FF", "P":"#004CBF", "As":"#999999", 
	"O":"#FF0000", "S":"#FED300", "Ge":"#BF5600", "F":"#95B338", "Cl":"#0FAD31", "Br":"#BF0800", "I":"#373475"
};

TME.Atom = function(symbol, coords) {
	var me = this;
	me.symbol = symbol;
	me.coords = coords;
	me.charge = 0;
	
	me.toString = function() {
		return this.symbol + "@" + this.coords;
	}

	me.flash = function(ctx, offset, scale, color) {
		ctx.strokeStyle = color;
		ctx.fillStyle = color;
		
		var delta = this.coords;
		var pos = new TME.XY(delta.x*scale + offset.x, offset.y - scale*delta.y);
		ctx.beginPath();
		ctx.arc(pos.x, pos.y, scale/3, 0, Math.PI*2,true);
		ctx.closePath();
		ctx.fill();
	}

	me.hit = function(pt, offset, scale) {
		var delta = this.coords;
		var pos = new TME.XY(delta.x*scale + offset.x, offset.y - scale*delta.y);
		if (pos.subBy(pt).abs() < (scale/3)) {
			return this;
		}
		else {
			return null;
		}
	}

	me.range = function(ctx, mol, offset, scale, fontSize) {
		var me = this;
		var symbol = me.symbol;
		if (symbol != "C" || mol.degree(me) == 0 || me.charge != 0) {
			var delta = me.coords;
		    ctx.font = "" + fontSize + "px arial";
		    var size = ctx.measureText(me.symbol);
		    size.height = fontSize*0.8;
		    var result = { x1: delta.x*scale + offset.x - size.width/2 - 1,
		    	y2: offset.y - scale*delta.y + fontSize*0.4 + 1};
		    result.x2 = result.x1 + size.width + 2;
		    result.y1 = result.y2 - size.height - 2;
		    return result;
		}
		else {
			return null;
		}
	}
}

TME.SINGLE_BOND		= 10;
TME.AROMATIC_BOND 	= 15;
TME.DOUBLE_BOND 	= 20;
TME.TRIPLE_BOND 	= 30;
TME.SQRT_2 	= Math.sqrt(2);
TME.HALF_SQRT_3 	= Math.sqrt(3)/2;
TME.ANGLE_30		= Math.PI / 6;

TME.Bond = function (atom1, atom2, order) {
	var me = this;
	me.atom1 = atom1;
	me.atom2 = atom2;
	me.order = order;
	me.stereo = 0;
	
	me.toString = function() {
		return "Bond of " + this.atom1.toString() + this.atom2.toString();
	}

	me.flash = function(ctx, offset, scale, color) {
		var delta1 = this.atom1.coords;
		var delta2 = this.atom2.coords;

		ctx.beginPath();
		ctx.moveTo(delta1.x*scale + offset.x, offset.y - scale*delta1.y);
		ctx.lineTo(delta2.x*scale + offset.x, offset.y - scale*delta2.y);
		ctx.stroke();
		ctx.closePath();
	}

	me.hit = function(pt, offset, scale) {
		var XY = TME.XY;
		var me = this;
		var atom1 = me.atom1, atom2 = me.atom2;
		var delta1 = atom1.coords, delta2 = atom2.coords;

		var x1 = delta1.x * scale + offset.x;
		var y1 = offset.y - scale * delta1.y;
		var x2 = delta2.x * scale + offset.x;
		var y2 = offset.y - scale * delta2.y;
		var shiftX = x2-x1;
		var shiftY = y2-y1;
		var delta = new XY(x2-x1, y2-y1);
		
		var ROTATE_FACTOR = 0.1*TME.SQRT_2;
		var delta1 = delta.mul(ROTATE_FACTOR, ROTATE_FACTOR);
		var pt1 = new XY(x1 + delta1.x, y1 + delta1.y);
		var delta2 = delta.mul(-ROTATE_FACTOR, ROTATE_FACTOR);
		var pt2 = new XY(x2 + delta2.x, y2 + delta2.y);
		

		delta1 = delta.mul(ROTATE_FACTOR, -ROTATE_FACTOR);
		var pt3 = new XY(x1 + delta1.x, y1 + delta1.y);
		delta2 = delta.mul(-ROTATE_FACTOR, -ROTATE_FACTOR);
		var pt4 = new XY(x2 + delta2.x, y2 + delta2.y);

		var side = TME.pt_side(pt1, pt2, pt);
		if (TME.pt_side(pt2, pt4, pt) != side) {
			return null;
		}
		if (TME.pt_side(pt4, pt3, pt) != side) {
			return null;
		}
		if (TME.pt_side(pt3, pt1, pt) != side) {
			return null;
		}

		return me;
	}
		

	me.__drawLine = function(ctx, pt1, pt2, rect1, rect2) {
		if (rect1 != null) {
			pt1 = TME.__intersectRectangle(pt1, pt2, rect1);
		}
		if (rect2 != null) {
			pt2 = TME.__intersectRectangle(pt2, pt1, rect2);
		}

		ctx.moveTo(pt1.x, pt1.y);
		ctx.lineTo(pt2.x, pt2.y);
	}

	me.drawSingleBond = function(ctx, mol, offset, scale, fontSize) {
		var XY = TME.XY;
		var me = this;
		var atom1 = me.atom1, atom2 = me.atom2;
		var delta1 = atom1.coords, delta2 = atom2.coords;

		var rect1 = atom1.range(ctx, mol, offset, scale, fontSize);
		var rect2 = atom2.range(ctx, mol, offset, scale, fontSize);

		var pt1 = new XY(delta1.x*scale + offset.x, offset.y - scale*delta1.y);
		var pt2 = new XY(delta2.x*scale + offset.x, offset.y - scale*delta2.y);
		switch (me.stereo) {
		case 1:
			var delta = pt2.sub(pt1)
			delta.mulBy(0, 0.08);
			var pt3 = pt2.add(delta)
			var pt4 = pt2.sub(delta)
			ctx.stroke();
			ctx.closePath();
			
			if (rect2 != null) {
				pt3 = TME.__intersectRectangle(pt3, pt1, rect2);
				pt4 = TME.__intersectRectangle(pt4, pt1, rect2);
			}
			if (rect1 != null) {
				pt2 = TME.__intersectRectangle(pt1, pt4, rect1);
				pt1 = TME.__intersectRectangle(pt1, pt3, rect1);
			}
			else {
				pt2 = pt1;
			}
			ctx.beginPath();
			ctx.moveTo(pt1.x, pt1.y)
			ctx.lineTo(pt3.x, pt3.y)
			ctx.lineTo(pt4.x, pt4.y)
			ctx.lineTo(pt2.x, pt2.y)
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
			ctx.beginPath();
			break;

		case 6:
			if (rect1 != null) {
				pt1 = TME.__intersectRectangle(pt1, pt2, rect1);
			}
			if (rect2 != null) {
				pt2 = TME.__intersectRectangle(pt2, pt1, rect2);
			}
			
			var delta = pt2.sub(pt1)
			var shift = delta.mul(0, 0.08);
			var n = delta.abs() / ctx.lineWidth / 2;
			for (var i = 1; i < n; ++i) {
				var deltaI = pt1.add(delta.scale(i/n))
				var shift1 = shift.scale(i/n)
				var pt3 = deltaI.add(shift1)
				var pt4 = deltaI.sub(shift1)
				ctx.moveTo(pt3.x, pt3.y)
				ctx.lineTo(pt4.x, pt4.y)
			}
			break;

		case 4:
			if (rect1 != null) {
				pt1 = TME.__intersectRectangle(pt1, pt2, rect1);
			}
			if (rect2 != null) {
				pt2 = TME.__intersectRectangle(pt2, pt1, rect2);
			}
			var delta = pt2.sub(pt1)
			var shift = delta.mul(0, 0.08);

			var n = delta.abs() / ctx.lineWidth;
			ctx.moveTo(pt1.x, pt1.y)
			var factor = 1
			for (var i = 1; i < n; ++i) {
				var deltaI = pt1.add(delta.scale(i/n))
				var shift1 = shift.scale(factor*i/n)
				factor *= -1
				deltaI.addBy(shift1)
				ctx.lineTo(deltaI.x, deltaI.y)
			}
			break;

		default:
			me.__drawLine(ctx, pt1, pt2, rect1, rect2);
			break;
		}
	}

	me.drawUnknowBond = function(ctx, mol, offset, scale, fontSize) {
		var XY = TME.XY;
		var me = this;
		var atom1 = me.atom1, atom2 = me.atom2;
		var delta1 = atom1.coords, delta2 = atom2.coords;

		var rect1 = atom1.range(ctx, mol, offset, scale, fontSize);
		var rect2 = atom2.range(ctx, mol, offset, scale, fontSize);

		var pt1 = new XY(delta1.x*scale + offset.x, offset.y - scale*delta1.y);
		var pt2 = new XY(delta2.x*scale + offset.x, offset.y - scale*delta2.y);
		if (rect1 != null) {
			pt1 = TME.__intersectRectangle(pt1, pt2, rect1);
		}
		if (rect2 != null) {
			pt2 = TME.__intersectRectangle(pt2, pt1, rect2);
		}
		var delta = pt2.sub(pt1)
		var r = delta.abs()
		var n = Math.floor(r/ctx.lineWidth/4) * 2 + 1
		for (var i = 0; i < n; i += 2) {
			var pt3 = pt1.add(delta.scale(i/n))
			ctx.moveTo(pt3.x, pt3.y)
			pt3 = pt1.add(delta.scale((i+1)/n))
			ctx.lineTo(pt3.x, pt3.y)
		} 
	}

	me.drawDoubleBond = function(ctx, mol, offset, scale, fontSize) {
		var XY = TME.XY;
		var me = this;
		var atom1 = me.atom1, atom2 = me.atom2;
		var delta1 = atom1.coords, delta2 = atom2.coords;

		var ROTATE_FACTOR = 0.1*TME.SQRT_2;

		var rect1 = atom1.range(ctx, mol, offset, scale, fontSize);
		var rect2 = atom2.range(ctx, mol, offset, scale, fontSize);

		var x1 = delta1.x * scale + offset.x;
		var y1 = offset.y - scale * delta1.y;
		var x2 = delta2.x * scale + offset.x;
		var y2 = offset.y - scale * delta2.y;
		var shiftX = x2-x1;
		var shiftY = y2-y1;
		var delta = new XY(x2-x1, y2-y1);

		if (me.stereo == 0) {
			var direction = TME.getDoubleBondDirection(mol, me);
			
			if (direction == 0) {
				delta.mulBy(0, 0.08);
				var pt1 = new XY(x1 + delta.x, y1 + delta.y);
				var pt2 = new XY(x2 + delta.x, y2 + delta.y);
				me.__drawLine(ctx, pt1, pt2, rect1, rect2);

				var pt1 = new XY(x1 - delta.x, y1 - delta.y);
				var pt2 = new XY(x2 - delta.x, y2 - delta.y);
				me.__drawLine(ctx, pt1, pt2, rect1, rect2);
			}
			else {
				var pt1 = new XY(x1, y1);
				var pt2 = new XY(x2, y2);
				me.__drawLine(ctx, pt1, pt2, rect1, rect2);

				if (direction == 1) {
					var delta1 = delta.mul(ROTATE_FACTOR, ROTATE_FACTOR);
					pt1 = new XY(x1 + delta1.x, y1 + delta1.y);
					var delta2 = delta.mul(-ROTATE_FACTOR, ROTATE_FACTOR);
					pt2 = new XY(x2 + delta2.x, y2 + delta2.y);
				}
				else {
					var delta1 = delta.mul(ROTATE_FACTOR, -ROTATE_FACTOR);
					pt1 = new XY(x1 + delta1.x, y1 + delta1.y);
					var delta2 = delta.mul(-ROTATE_FACTOR, -ROTATE_FACTOR);
					pt2 = new XY(x2 + delta2.x, y2 + delta2.y);
				}	
				me.__drawLine(ctx, pt1, pt2, rect1, rect2);
			}
		}
		else {
			delta = delta.mulBy(0, 0.08);
			var pt1 = new XY(x1 + delta.x, y1 + delta.y);
			var pt2 = new XY(x2 - delta.x, y2 - delta.y);
			me.__drawLine(ctx, pt1, pt2, rect1, rect2);

			var pt1 = new XY(x1 - delta.x, y1 - delta.y);
			var pt2 = new XY(x2 + delta.x, y2 + delta.y);
			me.__drawLine(ctx, pt1, pt2, rect1, rect2);
		}
	}

	me.drawTripleBond = function(ctx, mol, offset, scale, fontSize) {
		var ROTATE_FACTOR = 0.1*TME.SQRT_2;
		var XY = TME.XY;
		var me = this;
		var atom1 = me.atom1, atom2 = me.atom2;
		var delta1 = atom1.coords, delta2 = atom2.coords;

		var rect1 = atom1.range(ctx, mol, offset, scale, fontSize);
		var rect2 = atom2.range(ctx, mol, offset, scale, fontSize);

		var x1 = delta1.x * scale + offset.x;
		var y1 = offset.y - scale * delta1.y;
		var x2 = delta2.x * scale + offset.x;
		var y2 = offset.y - scale * delta2.y;
		var shiftX = x2-x1;
		var shiftY = y2-y1;
		var delta = new XY(x2-x1, y2-y1);
		
		var pt1 = new XY(x1, y1), pt2 = new XY(x2, y2);
		me.__drawLine(ctx, pt1, pt2, rect1, rect2);

		var delta1 = delta.mul(ROTATE_FACTOR, ROTATE_FACTOR);
		pt1.assign(x1 + delta1.x, y1 + delta1.y);
		var delta2 = delta.mul(-ROTATE_FACTOR, ROTATE_FACTOR);
		pt2.assign(x2 + delta2.x, y2 + delta2.y);
		me.__drawLine(ctx, pt1, pt2, rect1, rect2);


		delta1 = delta.mul(ROTATE_FACTOR, -ROTATE_FACTOR);
		pt1.assign(x1 + delta1.x, y1 + delta1.y);
		delta2 = delta.mul(-ROTATE_FACTOR, -ROTATE_FACTOR);
		pt2.assign(x2 + delta2.x, y2 + delta2.y);
		me.__drawLine(ctx, pt1, pt2, rect1, rect2);		
	}

	me.drawAromaticBond = function(ctx, mol, offset, scale, fontSize) {
		var XY = TME.XY;
		var me = this;
		var atom1 = me.atom1, atom2 = me.atom2;
		var delta1 = atom1.coords, delta2 = atom2.coords;

		var ROTATE_FACTOR = 0.1*TME.SQRT_2;

		var rect1 = atom1.range(ctx, mol, offset, scale, fontSize);
		var rect2 = atom2.range(ctx, mol, offset, scale, fontSize);

		var direction = TME.getDoubleBondDirection(mol, me);

		var x1 = delta1.x * scale + offset.x;
		var y1 = offset.y - scale * delta1.y;
		var x2 = delta2.x * scale + offset.x;
		var y2 = offset.y - scale * delta2.y;
		var shiftX = x2-x1;
		var shiftY = y2-y1;
		var delta = new XY(x2-x1, y2-y1);

		var pt1 = new XY(x1, y1);
		var pt2 = new XY(x2, y2);
		me.__drawLine(ctx, pt1, pt2, rect1, rect2);

		if (direction == 1) {
			var delta1 = delta.mul(ROTATE_FACTOR, ROTATE_FACTOR);
			pt1 = new XY(x1 + delta1.x, y1 + delta1.y);
			var delta2 = delta.mul(-ROTATE_FACTOR, ROTATE_FACTOR);
			pt2 = new XY(x2 + delta2.x, y2 + delta2.y);
		}
		else {
			var delta1 = delta.mul(ROTATE_FACTOR, -ROTATE_FACTOR);
			pt1 = new XY(x1 + delta1.x, y1 + delta1.y);
			var delta2 = delta.mul(-ROTATE_FACTOR, -ROTATE_FACTOR);
			pt2 = new XY(x2 + delta2.x, y2 + delta2.y);
		}
		
		delta = pt2.sub(pt1);
		var pt3 = pt1.add(delta.scale(0.4));
		var pt4 = pt1.add(delta.scale(0.6));
		me.__drawLine(ctx, pt1, pt3, rect1, null);
		me.__drawLine(ctx, pt4, pt2, null, rect2);
	}
}

TME.Molecule = function(atoms, bonds) {
	var me = this;
	me.atoms = atoms;
	me.bonds = bonds;
	var adjs = me.__adjs = [];
	for (var i in me.atoms) {
		adjs.push([]);
	}
	for (var i in this.bonds) {
		var i1 = atoms.indexOf(bonds[i].atom1);
		var i2 = atoms.indexOf(bonds[i].atom2);
		adjs[i1].push(bonds[i].atom2);
		adjs[i1].push(bonds[i]);
		adjs[i2].push(bonds[i].atom1);
		adjs[i2].push(bonds[i]);
	}
	
	me.indexOf = function(obj) {
		var me = this;
		var result = me.atoms.indexOf(obj);
		if (result == -1) {
			result = me.bonds.indexOf(obj);
		}
		return result;
	}
	
	me.degree = function (atom) {
		var i1 = this.atoms.indexOf(atom);
		return this.__adjs[i1].length / 2;
	}
	
	me.countAtoms = function() {
		return this.atoms.length;
	}
	
	me.countBonds = function() {
		return this.bonds.length;
	}
	
	me.getBond = function(atom1, atom2) {
		var me = this;
		var i1 = me.indexOf(atom1);
		if (i1 >= 0) {
			var n = me.degree(atom1)
			for (var i = 0; i < n*2; i += 2) {
				if (me.__adjs[i1][i] === atom2) {
					return me.__adjs[i1][i+1];
				}
			}
		}
		return null;
	}
	
	me.getNeighborAtom = function(atom, j) {
		var me = this;
		var i1 = me.indexOf(atom);
		return me.__adjs[i1][j*2];
	}
	
	me.getNeighborBond = function(atom, j) {
		var me = this;
		var i1 = me.indexOf(atom);
		return me.__adjs[i1][j*2+1];
	}

	me.formula = function() {
		var me = this;
		var nC = 0;
		var nH = 0;
		var counter = {};
		var symbols = [];
		for (var i in me.atoms) {
			var sym = me.atoms[i].symbol;
			if (sym == "C") {
				nC += 1;
			}
			else if (sym == "H") {
				nH += 1;
			}
			else if (sym in counter) {
				counter[sym] += 1;
			}
			else {
				counter[sym] = 1;
				symbols.push(sym);
			}
		}
		var result = [];
		if (nC > 0) {
			result.push("C");
			if (nC > 1) {
				result.push(nC);
			}
		}
		if (nH > 0) {
			result.push("H");
			if (nH > 1) {
				result.push(nH);
			}
		}
		symbols.sort();
		for (var i in symbols) {
			var sym = symbols[i];
			result.push(sym);
			var n = counter[sym];
			if (n > 1) {
				result.push(n);
			}
		}
		
		return result.join('');
	}
	
	me.size = function() {
		var atoms = this.atoms;
		if (atoms.length == 0) {
			return new TME.XY(0, 0);
		}
		else {
			var x1, x2, y1, y2;
			x1 = x2 = atoms[0].coords.x;
			y1 = y2 = atoms[0].coords.y;
			
			for (var i in atoms) {
				if (atoms[i].coords.x < x1) {
					x1 = atoms[i].coords.x;
				}
				else if (atoms[i].coords.x > x2) {
					x2 = atoms[i].coords.x;
				}
				if (atoms[i].coords.y < y1) {
					y1 = atoms[i].coords.y;
				}
				else if (atoms[i].coords.y > y2) {
					y2 = atoms[i].coords.y;
				}
			}
			return new TME.XY(x2-x1, y2-y1);
		}
	}
	me.center = function() {
		var atoms = this.atoms;
		if (atoms.length == 0) {
			return new TME.XY(0, 0);
		}
		else {
			var x1, x2, y1, y2;
			x1 = x2 = atoms[0].coords.x;
			y1 = y2 = atoms[0].coords.y;
			
			for (var i in this.atoms) {
				if (atoms[i].coords.x < x1) {
					x1 = atoms[i].coords.x;
				}
				else if (atoms[i].coords.x > x2) {
					x2 = atoms[i].coords.x;
				}
				if (atoms[i].coords.y < y1) {
					y1 = atoms[i].coords.y;
				}
				else if (atoms[i].coords.y > y2) {
					y2 = atoms[i].coords.y;
				}
			}
			return new TME.XY((x2+x1)/2, (y2+y1)/2);
		}
	}
	
	me.toString = function() {
		var atoms = this.atoms;
		var bonds = this.bonds;
		var result=["", "TinyMol Molecule", ""];
		
		var line = [TME.fixed_3(atoms.length), TME.fixed_3(bonds.length), 
			"  0  0  0  0  0  0  0  0999 V2000"];
		result.push(line.join(''));

		var charges = []
		
		for (var i in atoms) {
			var atom_i = atoms[i];
			line = [TME.fixed_float(atom_i.coords.x),
				TME.fixed_float(atom_i.coords.y),
				'    0.0000 ', atom_i.symbol];
			switch(atom_i.symbol.length) {
			case 0:
				line.push('   ');
				break;
			case 1:
				line.push('  ');
				break;
			case 2:
				line.push(' ');
				break;
			default:
				break;
			}
			line.push(' 0') // For mass
			if (-4 <= atom_i.charge && atom_i.charge < 4 && atom_i.charge != 0) {
				line.push(4-atom_i.charge)
			}
			else {
				line.push('  0')
			}
			if (atom_i.charge != 0) {
				charges.push(i)
			}
			
			line.push('  0  0  0  0  0  0  0  0  0  0');
			result.push(line.join(''));
		}
		
		for (var i in bonds) {
			var bond_i = bonds[i];
			line = [TME.fixed_3(this.indexOf(bond_i.atom1)+1),
				TME.fixed_3(this.indexOf(bond_i.atom2)+1)]
			switch(bond_i.order) {
			case TME.SINGLE_BOND:
				line.push(TME.fixed_3(1));
				break;
			case TME.DOUBLE_BOND:
				line.push(TME.fixed_3(2));
				break;
			case TME.TRIPLE_BOND:
				line.push(TME.fixed_3(3));
				break;
			case TME.AROMATIC_BOND:
				line.push(TME.fixed_3(4));
				break;
			default:
				line.push(TME.fixed_3(8));
				break;
			}
			line.push(TME.fixed_3(bond_i.stereo));
			line.push('  0  0  0');
			result.push(line.join(''));
		}

		for (var i = 0; i < charges.length; i += 8) {
			line = ['M  CHG']
			var remains =  charges.length - i
			if (remains > 8) {
				remains = 8;
			}
			line.push(TME.fixed_3(remains))
			for (var j = 0; j < remains; ++j) {
				var atom_i = charges[i+j]
				line.push(TME.fixed_4(atom_i+1))
				atom_i = atoms[atom_i]
				line.push(TME.fixed_4(atom_i.charge))
			}
			result.push(line.join(''));
		}
		
		result.push('M  END');
		return result.join('\n');
	}
	
	me.toSmiles = function() {
		return "TODO:";
	}
	
	me.addAtom = function(a) {
		var me = this;
		if (me.indexOf(a) == -1) {
			me.atoms.push(a);
			me.__adjs.push([]);
			return true;
		}
		else {
			return false;
		}
	}

	me.addBond = function(b) {
		var me = this;
		if (me.indexOf(b) == -1) {
			var atom1 = me.indexOf(b.atom1);
			var atom2 = me.indexOf(b.atom2);
			if (atom1 == -1) {
				atom1 = me.atoms.length;
				me.addAtom(b.atom1);
			}
			if (atom2 == -1) {
				atom2 = me.atoms.length;
				me.addAtom(b.atom2);
			}
			me.bonds.push(b);
			me.__adjs[atom1].push(b.atom2);
			me.__adjs[atom1].push(b);
			me.__adjs[atom2].push(b.atom1);
			me.__adjs[atom2].push(b);
			return true;
		}
		else {
			return false;
		}
	}
	
	me.flash = function(ctx, offset, scale, color) {
		var me = this;
		for (var i in me.atoms) {
			me.atoms[i].flash(ctx, scale, offset, color);
		}
		for (var i in me.bonds) {
			me.bonds[i].flash(ctx, scale, offset, color);	
		}
	}

	me.hit = function(pt, offset, scale) {
		var me = this;
		for (var i in me.atoms) {
			var result = me.atoms[i].hit(pt, offset, scale);
			if (result != null) {
				return result;
			}
		}
		
		for (var i in me.bonds) {
			var result = me.bonds[i].hit(pt, offset, scale);
			if (result != null) {
				return result;
			}	
		}

		return null;
	}

	me.drop_objs = function(atoms, bonds) {
		var new_atoms = [], new_bonds = []
		for (var i in me.atoms) {
			var atomI = me.atoms[i]
			if (atoms.indexOf(atomI) == -1) {
				new_atoms.push(atomI)
			}
		}
		for (var i in me.bonds) {
			var bondI = me.bonds[i]
			if (bonds.indexOf(bondI) == -1 && atoms.indexOf(bondI.atom1) == -1 && atoms.indexOf(bondI.atom2) == -1) {
				new_bonds.push(bondI)
			}
		}

		me.atoms = atoms = new_atoms;
		me.bonds = bonds = new_bonds;
		var adjs = me.__adjs = [];
		for (var i in me.atoms) {
			adjs.push([]);
		}
		for (var i in me.bonds) {
			var b = me.bonds[i];
			var atom1 = me.indexOf(b.atom1);
			var atom2 = me.indexOf(b.atom2);
			adjs[atom1].push(bonds[i].atom2);
			adjs[atom1].push(bonds[i]);
			adjs[atom2].push(bonds[i].atom1);
			adjs[atom2].push(bonds[i]);
		}
	}

	me.near_atom = function(pt, r) {
		for (var i in me.atoms) {
			var atomI = me.atoms[i];
			var f = atomI.coords.sub(pt).abs();
			if (f < r) {
				return atomI;
			}			
		}
		return null;
	}
}

TME.findMinRing = function(mol, bond) {
	var visited = {};
	visited[bond.atom1] = bond.atom2;
	visited[bond.atom2] = bond.atom1;
	var queue = [];
	queue.push(bond.atom2);
	while (queue.length > 0) {
		var top = queue.shift();
		var deg = mol.degree(top);
		for (var i = 0; i < deg; ++i) {
			var atomI = mol.getNeighborAtom(top, i);
			if (top != bond.atom2 && atomI == bond.atom1) {
				result = [];
				result.push(bond.atom1);
				while (top != bond.atom2) {
					result.push(top);
					top = visited[top];
				}
				result.push(bond.atom2);
				return result;
			}
			if (typeof(visited[atomI]) == 'undefined') {
				visited[atomI] = top;
				queue.push(atomI);
			}
		}
	}
	return null;
}

TME.parseMol = function(content) {
	var tokens = content.split("\n");
	if (tokens.length < 4) {
		return null;
	}
	var natoms = tokens[3].substring(0, 3)-0;
	var nbonds = tokens[3].substring(3, 6)-0;
	
	if (tokens.length < natoms + nbonds + 4) {
		return null;
	}
	var atoms = [];
	for (var i = 0; i < natoms; ++i) {
		var line = tokens[4+i];
		var x = line.substring(0, 10)-0;
		var y = line.substring(10, 20)-0;
		var z = line.substring(20, 30)-0;
		var symbol = line.substring(30, 33).trim();
		var atom_i = new TME.Atom(symbol, new TME.XY(x, y));
		var charge = line.substring(36, 39).trim()
		if (charge != 0) {
			charge = 4 - charge;
			if (charge == 0) {
				charge = ':'
			}
			atom_i.charge = charge;
		}
		atoms.push(atom_i);
	}
	var bonds = [];
	for (i = 0; i < nbonds; ++i) {
		var line = tokens[4+natoms+i];
		var atom1 = line.substring(0, 3)-1;
		var atom2 = line.substring(3, 6)-1;
		var order = line.substring(6, 9)-0;
		var stereo = line.substring(9, 12)-0;
		switch (order) {
		case 1:
			order = TME.SINGLE_BOND;
			break;
		case 2:
			order = TME.DOUBLE_BOND;
			break;
		case 3:
			order = TME.TRIPLE_BOND;
			break;
		case 4:
			order = TME.AROMATIC_BOND;
			break;
		default:
			order = 0;
			break;
		}
		var bond_i = new TME.Bond(atoms[atom1], atoms[atom2], order);
		bonds.push(bond_i);
		bond_i.stereo = stereo;
	}
	for (i = 4+natoms+nbonds; i < tokens.length; ++i) {
		var line = tokens[i];
		if (line.substring(0, 6) == 'M  CHG') {
			for (var j = 9; j < line.length; j += 8) {
				var atom_j = jQuery.trim(line.substring(j, j+4)) - 1
				var q = jQuery.trim(line.substring(j+4, j+8))
				atoms[atom_j].charge = q
			}
		}
	}
	return new TME.Molecule(atoms, bonds);
}

TME.pt_side = function(v1, v2, pt) {
	var ba = v2.sub(v1);
	var pa = pt.sub(v1);
	var side = pa.x*ba.y - pa.y*ba.x;
	if (side > 0) {
		return 1;
	}
	else {
		return -1;
	}
}

TME.atomside = function (bond, atomP) {
	return TME.pt_side(bond.atom1.coords, bond.atom2.coords, atomP.coords);
}

TME.getDoubleBondDirection = function(mol, bond) {
	if (mol.degree(bond.atom1) == 1 && mol.degree(bond.atom2) == 1) {
		return 0;
	}
	else if (mol.degree(bond.atom1) == 1 && mol.degree(bond.atom2) == 3) {
		return 0;
	}
	else if (mol.degree(bond.atom1) == 3 && mol.degree(bond.atom2) == 1) {
		return 0;
	}
	else if (mol.degree(bond.atom1) == 1) {
		var deg = mol.degree(bond.atom2);
		var side = 0;
		for (var i = 0; i < deg; ++i) {
			atomI = mol.getNeighborAtom(bond.atom2, i);
			if (atomI != bond.atom1) {
				side += TME.atomside(bond, atomI);
			}
		}
		if (side > 0) {
			return 1;
		}
		else {
			return -1;
		}
	}
	else if (mol.degree(bond.atom2) == 1) {
		var deg = mol.degree(bond.atom1);
		var side = 0;
		for (var i = 0; i < deg; ++i) {
			var atomI = mol.getNeighborAtom(bond.atom1, i);
			if (atomI != bond.atom2) {
				side += TME.atomside(bond, atomI);
			}
		}
		if (side > 0) {
			return 1;
		}
		else {
			return -1;
		}
	}
	else {
		var ring = TME.findMinRing(mol, bond);
		if (ring == null) {
			var side = 0;
			var deg = mol.degree(bond.atom1);
			var side = 0;
			for (var i = 0; i < deg; ++i) {
				var atomI = mol.getNeighborAtom(bond.atom1, i);
				if (atomI != bond.atom2) {
					side += TME.atomside(bond, atomI);
				}
			}
			deg = mol.degree(bond.atom2);
			for (var i = 0; i < deg; ++i) {
				var atomI = mol.getNeighborAtom(bond.atom2, i);
				if (atomI != bond.atom1) {
					side += TME.atomside(bond, atomI);
				}
			}
			if (side > 0) {
				return 1;
			}
			else {
				return -1;
			}
		}
		else {
			return TME.atomside(bond, ring[1]);
		}
	}
}

TME.getMiddleOfBondLength  = function(mol) {
	
	var bondLength = [];
	var r = 1;
	for (var i in mol.bonds) {
		bondI = mol.bonds[i];
		r = bondI.atom1.coords.sub(bondI.atom2.coords).abs();
		bondLength.push(r);
	}
	bondLength.sort();
	
	if (bondLength.length == 0) {
		r = 1;
	}
	else if (bondLength.length % 2 == 1) {
		r = bondLength[(bondLength.length-1) / 2];
	}
	else {
		var v1 = bondLength.length / 2;
		r = (bondLength[v1-1] + bondLength[v1]) / 2;
	}
	return r;
}


TME.drawMolOnContext = function(ctx, mol, offset, scale) {
	var defaultColor = ctx.strokeStyle;
	var defaultBackgd = ctx.fillStyle;

	var r = TME.getMiddleOfBondLength(mol);
	var fontSize = Math.floor(r*scale*0.5);
	var linewidth = fontSize / 6;
	if (linewidth < 1) {
		linewidth = 1;
	}
	else {
		linewidth = Math.floor(linewidth);
	}
	
	var XY = TME.XY;
	var ROTATE_FACTOR = 0.1*TME.SQRT_2;

	ctx.lineWidth = linewidth;
	ctx.beginPath();
	for (var i in mol.bonds) {
		var bondI = mol.bonds[i];
		switch (bondI.order) {
		case TME.SINGLE_BOND:
			bondI.drawSingleBond(ctx, mol, offset, scale, fontSize);
			break;
			
		case TME.DOUBLE_BOND:
			bondI.drawDoubleBond(ctx, mol, offset, scale, fontSize);
			break;
			
		case TME.TRIPLE_BOND:
			bondI.drawTripleBond(ctx, mol, offset, scale, fontSize);
			break;
			
		case TME.AROMATIC_BOND:
			bondI.drawAromaticBond(ctx, mol, offset, scale, fontSize);
			break;

		default:
			bondI.drawUnknowBond(ctx, mol, offset, scale, fontSize);
			break;
		}
	}
	ctx.stroke();
	ctx.closePath();
	
	var ATOM_COLORS = TME.ATOM_COLORS;
	for (var i in mol.atoms) {
		var atom = mol.atoms[i];
		var symbol = atom.symbol;
		if (symbol != "C" || mol.degree(atom) == 0 || atom.charge != 0) {
		    var delta = atom.coords;
		    ctx.font = "" + fontSize + "px arial";
		    var size = ctx.measureText(atom.symbol);
		    size.height = fontSize*0.8;
		    var pos = new XY(delta.x*scale + offset.x - size.width/2, offset.y - scale*delta.y + size.height/2);
		    
		    if (symbol in ATOM_COLORS) {
		    	ctx.fillStyle = ATOM_COLORS[symbol];
		    }
		    else {
		    	ctx.fillStyle = defaultColor;
		    }
		    ctx.fillText(atom.symbol, pos.x, pos.y);

		    var q = null
		    if (atom.charge > 0) {
		    	q = "+"
		    	if (atom.charge > 1) {
		    		q = atom.charge + "+"
		    	}
		    }
		    else if (atom.charge < 0) {
		    	q = "-"
		    	if (atom.charge < -1) {
		    		q = -atom.charge + "–"
		    	}
		    }
		    if (q != null) {
		    	ctx.font = "" + (fontSize*0.6) + "px arial";
		    	pos = new XY(delta.x*scale + offset.x + size.width/2, offset.y - scale*delta.y);
		    	ctx.fillText(q, pos.x, pos.y);
		    }
		}
	}
	ctx.strokeStyle = defaultColor;
	ctx.fillStyle = defaultBackgd;
}

TME.flashOnContext = function(ctx, objs, offset, scale, color) {
	var defaultColor = ctx.strokeStyle;
	var defaultBackgd = ctx.fillStyle;
	var defaultLineWidth = ctx.lineWidth;

	ctx.strokeStyle = color;
	ctx.fillStyle = color;
	ctx.lineWidth = scale/3;

	for (var i in objs) {
		objs[i].flash(ctx, offset, scale, color);
	}
	ctx.strokeStyle = defaultColor;
	ctx.fillStyle = defaultBackgd;
	ctx.lineWidth = defaultLineWidth;
}

TME.drawMol = function(canvas, mol, flash_objs, margin) {
	if (typeof(margin) == 'undefined')  {
		margin = 1
	}
	margin = margin*TME.getMiddleOfBondLength(mol);
	var size = mol.size();
	var wfactor = canvas.width / (size.x+margin);
	var hfactor = canvas.height / (size.y+margin);
	var factor = 0
	if (wfactor > hfactor) {
		factor = hfactor;
	}
	else {
		factor = wfactor;
	}
	var center = new TME.XY(canvas.width/2, canvas.height/2);
	var ctx = canvas.getContext("2d");
	ctx.fillStyle = ctx.strokeStyle = "#000000";
	ctx.lineCap = "round";

	var offset = mol.center();
	offset.x *= -factor;
	offset.y *= factor;
	offset.addBy(center);
	// offset.addBy(0, factor/2);


	ctx.clearRect(0, 0, canvas.width, canvas.height);
	if (typeof(flash_objs) != "undefined") {
		for (var color in flash_objs) {
			TME.flashOnContext(ctx, flash_objs[color], offset, factor, color)
		}
	}

    TME.drawMolOnContext(ctx, mol, offset, factor);
}

TME.ActionNewMolecule = function(editor, new_mol, zoom0) {
	var me = this;
	me.editor = editor;
	me.new_mol = new_mol;
	me.old_mol = editor.mol;
	me.old_bondLength = editor.bondLength;
	me.old_offset = editor.offset;
	me.old_factor = editor.factor;
	if (typeof(zoom0) == 'undefined') {
		zoom0 = true
	}
	me.zoom0 = zoom0;

	me.undo = function() {
		var me = this;
		me.editor.mol = me.old_mol;
		me.editor.bondLength = me.old_bondLength;
		me.editor.offset = me.old_offset;
		me.editor.factor = me.old_factor;
	}

	me.redo = function() {
		var me = this;
		me.editor.mol = me.new_mol;
		if (me.zoom0) {
			me.editor.zoom0();
		}
	}
}

TME.ActionNewAtom = function(editor, atom) {
	var me = this;
	me.editor = editor;
	me.atom = atom;
	
	me.undo = function() {
		this.editor.mol.drop_objs([this.atom], []);
	}
	me.redo = function() {
		var mol = this.editor.mol; 
		mol.atoms.push(this.atom);
		mol.__adjs.push([]);
	}
}

TME.ActionEditAtom = function(atom, symbol) {
	var me = this;
	me.atom = atom;
	me.old_symbol = atom.symbol;
	me.symbol = symbol;

	me.undo = function() {
		this.atom.symbol = this.old_symbol;
	}
	me.redo = function() {
		this.atom.symbol = this.symbol;
	}
}

TME.ActionEditCharge = function(atom, charge) {
	var me = this;
	me.atom = atom;
	me.charge = charge;

	me.undo = function() {
		this.atom.charge -= this.charge;
	}
	me.redo = function() {
		this.atom.charge += this.charge;
	}
}

TME.ActionNewBond = function(mol, bond, atom1, atom2) {
	var me = this;
	me.mol = mol;
	me.bond = bond;
	me.atom1 = atom1;
	me.atom2 = atom2;

	me.undo = function() {
		var me = this;
		var atoms = [];
		if (me.atom2 != null) {
			atoms.push(me.atom2);
		}
		if (me.atom1 != null) {
			atoms.push(me.atom1);
		}
		mol.drop_objs(atoms, [me.bond])
	}

	me.redo = function() {
		var me = this;
		if (me.atom1 != null) {
			mol.addAtom(me.atom1);
		}
		if (me.atom2 != null) {
			mol.addAtom(me.atom2);
		}
		mol.addBond(me.bond);
	}
}

TME.ActionEditBond = function(bond, order, stereo) {
	var me = this;
	me.bond = bond;
	me.order = order;
	me.stereo = stereo;
	me.old_order = bond.order;
	me.old_stereo = bond.stereo;

	me.undo = function() {
		me.bond.stereo = me.old_stereo;
		me.bond.order = me.old_order;
	}
	me.redo = function() {
		me.bond.stereo = me.stereo;
		me.bond.order = me.order;
	}
}

TME.ActionEraseObjects = function(editor, atoms, bonds) {
	var me = this;
	me.editor = editor;
	var mol = me.old_mol = editor.mol;
	var atoms_new = [], bonds_new = [];
	for (var i in mol.atoms) {
		var atom_i = mol.atoms[i];
		if (atoms.indexOf(atom_i) == -1) {
			atoms_new.push(atom_i);
		}
	}
	for (var i in mol.bonds) {
		var bond_i = mol.bonds[i];
		if (bonds.indexOf(bond_i) == -1) {
			bonds_new.push(bond_i);
		}
	}

	me.new_mol = new TME.Molecule(atoms_new, bonds_new);

	me.undo = function() {
		var me = this;
		me.editor.mol = me.old_mol;
	}

	me.redo = function() {
		var me = this;
		me.editor.mol = me.new_mol;
	}
}

TME.HandleEraser = function(editor) {
	var me = this;
	me.editor = editor;
	me.pt_down = me.obj_down = null;
	me.old_hit_obj = null;

	me.__bonds_of_atoms = function(mol, atoms) {
		var bonds = []
		for (var i in atoms) {
			var atom_i = atoms[i];
			var n = mol.degree(atom_i);
			for (var j = 0; j < n; ++j) {
				var bond_j = mol.getNeighborBond(atom_i, j);
				if (bonds.indexOf(bond_j) == -1) {
					bonds.push(bond_j)	
				}
			}
		}
		return bonds;
	}

	var canvas = editor.canvas;
	canvas.unbind("mousemove");
	canvas.unbind("mouseup");
	canvas.unbind("mousedown");

	canvas.mousedown(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		handler.pt_down = editor.get_mouse_position(evt);
		handler.obj_down = editor.mol.hit(handler.pt_down, editor.offset, editor.factor);
	});

	canvas.mousemove(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		var mol = editor.mol;
		var pt_mouse = editor.get_mouse_position(evt);
		if (handler.pt_down == null) {
			var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
			if (new_hit_obj != handler.old_hit_obj) {
				handler.old_hit_obj = new_hit_obj;
				if (new_hit_obj == null) {
					editor.draw({});
				}
				else {
					
					var objs = [new_hit_obj];
					if (new_hit_obj instanceof TME.Atom) {
						for (var i = 0; i < mol.degree(new_hit_obj); ++i) {
							objs.push(mol.getNeighborBond(new_hit_obj, i));
						}

					}
					var highlights = {};
					highlights[editor.warning_color] = objs;
					editor.draw(highlights);
				}
			}
		}
		else {
			var highlights = {};
			var objs = editor.atoms_in_rectangle(handler.pt_down, pt_mouse);
			var bonds = handler.__bonds_of_atoms(mol, objs);

			for (var i in bonds) {
				objs.push(bonds[i]);
			}
			highlights[editor.warning_color] = objs;
			editor.draw(highlights);

			var ctx = this.getContext("2d");
			var backup_color = ctx.strokeStyle;
			var backup_width = ctx.lineWidth;

			ctx.strokeStyle = editor.warning_color;
			ctx.lineWidth = 1;

			ctx.beginPath();
			ctx.moveTo(handler.pt_down.x, handler.pt_down.y);
			ctx.lineTo(pt_mouse.x, handler.pt_down.y);
			ctx.lineTo(pt_mouse.x, pt_mouse.y);
			ctx.lineTo(handler.pt_down.x, pt_mouse.y);
			ctx.lineTo(handler.pt_down.x, handler.pt_down.y);
			ctx.stroke();
			ctx.closePath();

			ctx.strokeStyle = backup_color;
			ctx.lineWidth = backup_width;
		}
	});

	canvas.mouseup(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		var mol = editor.mol;
		
		if (handler.pt_down != null) {
			var pt_mouse = editor.get_mouse_position(evt);
			var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
			var atoms = [], bonds = [];
			if (new_hit_obj != null && new_hit_obj == handler.obj_down) {
				if (new_hit_obj instanceof TME.Atom) {
					atoms = [new_hit_obj];
					bonds = handler.__bonds_of_atoms(mol, atoms);
				}
				else {
					atoms = []
					bonds = [new_hit_obj];
				}
			}
			else {
				atoms = editor.atoms_in_rectangle(handler.pt_down, pt_mouse);
				bonds = handler.__bonds_of_atoms(mol, atoms);
			}
		}

		if (atoms.length > 0 || bonds.length > 0) {
			var action = new TME.ActionEraseObjects(editor, atoms, bonds);
			editor.doit(action);
		}
		editor.draw({});
		handler.pt_down =  handler.obj_down = null;
	});
}

TME.HandleEditAtom = function(editor, symbol) {
	var me = this;
	me.editor = editor;
	me.symbol = symbol;
	me.old_hit_obj = null;

	var canvas = editor.canvas;
	canvas.unbind("mousemove");
	canvas.unbind("mouseup");
	canvas.unbind("mousedown");

	canvas.mousemove(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		var mol = editor.mol;
		var pt_mouse = editor.get_mouse_position(evt);
		var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
		if (new_hit_obj != handler.old_hit_obj) {
			handler.old_hit_obj = new_hit_obj;
			if (new_hit_obj == null) {
				editor.draw({});
			}
			else {
				var highlights = {};
				highlights[editor.flash_color] = [new_hit_obj];
				editor.draw(highlights);
			}
		}
	});

	canvas.mouseup(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		var mol = editor.mol;
		
		var pt_mouse = editor.get_mouse_position(evt);
		var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
		
		var action = null;
		var atom = null;
		if (new_hit_obj == null) {
			var coords = editor.canvas_to_world(pt_mouse);
			atom = new TME.Atom(handler.symbol, coords);
			action = new TME.ActionNewAtom(editor, atom);
		}
		else {
			if (new_hit_obj instanceof TME.Atom) {
				atom = new_hit_obj;
				if (atom.symbol != handler.symbol) {
					action = new TME.ActionEditAtom(atom, handler.symbol);
				}
			}
		}

		if (action != null) {
			editor.doit(action);
			var highlights = {};
			highlights[editor.flash_color] = [atom];
			editor.draw(highlights);
		}
	});
}


TME.HandleEditCharge = function(editor, charge) {
	var me = this;
	me.editor = editor;
	me.charge = charge;
	me.old_hit_obj = null;

	var canvas = editor.canvas;
	canvas.unbind("mousemove");
	canvas.unbind("mouseup");
	canvas.unbind("mousedown");

	canvas.mousemove(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		var mol = editor.mol;
		var pt_mouse = editor.get_mouse_position(evt);
		var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
		if (new_hit_obj != handler.old_hit_obj) {
			handler.old_hit_obj = new_hit_obj;
			if (new_hit_obj == null) {
				editor.draw({});
			}
			else {
				var highlights = {};
				highlights[editor.flash_color] = [new_hit_obj];
				editor.draw(highlights);
			}
		}
	});

	canvas.mouseup(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		var mol = editor.mol;
		
		var pt_mouse = editor.get_mouse_position(evt);
		var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
		
		var action = null;
		var atom = null;
		if (new_hit_obj != null && new_hit_obj instanceof TME.Atom) {
			atom = new_hit_obj;
			action = new TME.ActionEditCharge(atom, handler.charge)
		}

		if (action != null) {
			editor.doit(action);
			var highlights = {};
			highlights[editor.flash_color] = [atom];
			editor.draw(highlights);
		}
	});
}

TME.HandleEditBond = function(editor, order, stereo) {
	var me = this;
	me.order = order;
	me.stereo = stereo;
	var handle = me;
	var hit_obj = null;
	var pt_down = null;
	var obj_down = null;
	var ghosts = null; 
	var old_ghosts = [null, null, null];
	var canvas = editor.canvas;

	canvas.unbind("mousemove");
	canvas.unbind("mouseup");
	canvas.unbind("mousedown");

	canvas.mousedown(function(evt) {
		pt_down = editor.get_mouse_position(evt);
		obj_down = editor.mol.hit(pt_down, editor.offset, editor.factor);
		
		var atoms = [new TME.Atom("C"), new TME.Atom("C")];
		var bond = new TME.Bond(atoms[0], atoms[1], order, stereo);
		ghosts = new TME.Molecule(atoms, [bond]);

		if (hit_obj instanceof TME.Atom) {
			ghosts.atoms[0].coords = hit_obj.coords;
		}
		else if (hit_obj == null) {
			ghosts.atoms[0].coords = editor.canvas_to_world(pt_down);
		}
		else {
			ghosts = null;
		}
	});

	canvas.mousemove(function(evt) {
		var pt_mouse = editor.get_mouse_position(evt);
		var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
		var need_redraw = false;
		if (new_hit_obj != hit_obj) {
			need_redraw = true;
			hit_obj = new_hit_obj;
		}

		if (ghosts != null) {
			ghosts.atoms[1].coords = null;
		}


		if (ghosts != null && obj_down != hit_obj && hit_obj instanceof TME.Atom) {
			ghosts.atoms[1].coords = hit_obj.coords;

			if (old_ghosts[0] != ghosts 
				|| old_ghosts[1] != ghosts.atoms[0].coords 
				|| old_ghosts[2] != ghosts.atoms[1].coords)
			{
				need_redraw = true;
				old_ghosts = [ghosts, ghosts.atoms[0].coords, ghosts.atoms[1].coords]
			}
		}

		if (pt_down != null && ghosts != null && ghosts.atoms[1].coords == null) {
			var delta = pt_mouse.sub(pt_down);
			var r = delta.abs();
			if (r > editor.bondLength*.3) {
				var ang = Math.ceil(Math.atan2(delta.y, delta.x) / TME.ANGLE_30 - .5);
				ang *= TME.ANGLE_30; 
				delta.assign(Math.cos(ang), -Math.sin(ang));
				delta.scaleBy(editor.bondLength);
				delta.addBy(ghosts.atoms[0].coords);
				ghosts.atoms[1].coords = delta;
				if (old_ghosts[0] != ghosts 
					|| old_ghosts[1] != ghosts.atoms[0].coords 
					|| old_ghosts[2] != ghosts.atoms[1].coords)
				{
					need_redraw = true;
					old_ghosts = [ghosts, ghosts.atoms[0].coords, ghosts.atoms[1].coords]
				}
			}
		}
		if (need_redraw) {
			if (hit_obj) {
				var highlights = {};
				highlights[editor.flash_color] = [hit_obj];
				editor.draw(highlights);
			}
			else {
				editor.draw({});
			}
		
			if (ghosts != null && ghosts.atoms[1].coords != null) {
				var ctx = this.getContext("2d");
				var backup_style = ctx.strokeStyle;
				ctx.strokeStyle = editor.flash_color;
				TME.drawMolOnContext(ctx, ghosts, editor.offset, editor.factor);
				ctx.strokeStyle = backup_style;
			}
		}
	});

	canvas.mouseup(function(evt) {
		var me = this.editor;
		var ActionEditBond = TME.ActionEditBond;
		var pt = me.get_mouse_position(evt);
		var hit_obj = me.mol.hit(pt, editor.offset, editor.factor);
		
		if (hit_obj == obj_down && hit_obj != null) {
			if (hit_obj instanceof TME.Atom) {
				var atom1 = hit_obj;
				var pt1 = atom1.coords;
				var d = me.mol.degree(atom1);
				var atom2 = null;
				var bond = null;
				switch (d) {
					case 0:
						var pt2 = pt1.add(me.bondLength*TME.HALF_SQRT_3, me.bondLength/2);
						atom2 = me.mol.near_atom(pt2, 0.2*me.bondLength);
						if (atom2 == null) {
							atom2 = new TME.Atom("C", pt2);
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
						}
						else {
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
							atom2 = null;
						}

					break;

					case 1:
						atom2 = me.mol.getNeighborAtom(atom1, 0);
						var delta = atom2.coords.sub(pt1).mul(-.5, TME.HALF_SQRT_3);
						var pt2 = pt1.add(delta);
						atom2 = me.mol.near_atom(pt2, 0.2*me.bondLength);
						if (atom2 == null) {
							atom2 = new TME.Atom("C", pt2);
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
						}
						else {
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
							atom2 = null;
						}

					break;

					case 2:
						atom2 = me.mol.getNeighborAtom(atom1, 0);
						var delta = atom2.coords.sub(pt1);
						delta.addBy(me.mol.getNeighborAtom(atom1, 1).coords.sub(pt1));
						var r = delta.abs();

						if (r < 0.1) {
							delta = atom2.coords.sub(pt1);
							delta.mulBy(0, -1);
							r = delta.abs();
						}
						
						delta.scaleBy(-me.bondLength/r);

						var pt2 = pt1.add(delta);
						atom2 = me.mol.near_atom(pt2, 0.2*me.bondLength);
						if (atom2 == null) {
							atom2 = new TME.Atom("C", pt2);
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
						}
						else {
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
							atom2 = null;
						}
					break;

					case 3:
						atom2 = me.mol.getNeighborAtom(atom1, 0);
						var delta = atom2.coords.sub(pt1);
						delta.mulBy(0, -1);
						var r = delta.abs();
						
						delta.scaleBy(-me.bondLength/r);
						var pt2 = pt1.add(delta);
						atom2 = me.mol.near_atom(pt2, 0.2*me.bondLength);
						if (atom2 == null) {
							atom2 = new TME.Atom("C", pt2);
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
						}
						else {
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
							atom2 = null;
						}
					break;

					default:
					for (var i = 0; i < d; ++i) {
						atom2 = me.mol.getNeighborAtom(atom1, i);
						var delta = atom2.coords.sub(pt1);
						delta.mulBy(TME.HALF_SQRT_3, -.5);
						var pt2 = pt1.add(delta);
						if (me.mol.near_atom(pt2, 0.3*me.bondLength) == null){
							atom2 = new TME.Atom("C", pt2);
							bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);	
							break;
						}
					}
					break;
				}

				if (bond != null && me.mol.getBond(bond.atom1, bond.atom2) == null) {
					var action = new TME.ActionNewBond(me.mol, bond, null, atom2);

					me.doit(action);

					var highlights = {};
					highlights[editor.flash_color] = [bond];
					if (atom2 != null) {
						highlights[editor.flash_color].push(atom2);
					}
					editor.draw(highlights);
				}
			}
			else if (hit_obj instanceof TME.Bond) {
				var action = null;
				if (stereo == 0) {
					switch (hit_obj.order) {
					case TME.SINGLE_BOND:
						action = new ActionEditBond(hit_obj, TME.DOUBLE_BOND, 0);
						break;
					case TME.DOUBLE_BOND: 
						action = new ActionEditBond(hit_obj, TME.TRIPLE_BOND, 0);
						break;
					case TME.TRIPLE_BOND:
						action = new ActionEditBond(hit_obj, TME.AROMATIC_BOND, 0);
						break;

					default:
						action = new ActionEditBond(hit_obj, TME.SINGLE_BOND, 0);
						break;
					}
				}
				else {
					action = new ActionEditBond(hit_obj, order, 0);
				}

				 
				me.doit(action);

				var highlights = {};
				highlights[editor.flash_color] = [hit_obj];
				editor.draw(highlights);
			}
		}
		else if (ghosts != null) {
			if (ghosts.atoms[1].coords == null) {
				pt = me.canvas_to_world(pt);
				var atom1 = new TME.Atom("C", pt);
				var pt2 = pt.add(me.bondLength*TME.HALF_SQRT_3, me.bondLength/2);
				var atom2 = me.mol.near_atom(pt2, 0.3*me.bondLength);

				if (atom2 == null) {
					atom2 = new TME.Atom("C", pt2);
					var bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
				}
				else {
					var bond = new TME.Bond(atom1, atom2, me.handler.order, me.handler.stereo);
					atom2 = null;
				}
				
				var action = new TME.ActionNewBond(me.mol, bond, atom1, atom2);

				me.doit(action);

				var flash_objs = [bond];
				if (atom1 != null) {
					flash_objs.push(atom1);
				}
				if (atom2 != null) {
					flash_objs.push(atom2);
				}
				var highlights = {}
				highlights[editor.flash_color] = flash_objs;
				editor.draw(highlights);
			}
			else {
				var atom1 = null, atom11 = null;
				var atom2 = null, atom22 = null; 
				if (obj_down instanceof TME.Atom) {
					atom1 = obj_down;
				}
				else {
					atom1 = atom11 = ghosts.atoms[0];
				}
				if (hit_obj instanceof TME.Atom) {
					atom2 = hit_obj;
				}
				else {
					atom2 = atom22 = ghosts.atoms[1];
				}
				if (me.mol.getBond(atom1, atom2) == null) {
					var bond = new TME.Bond(atom1, atom2, order, stereo);
					var action = new TME.ActionNewBond(me.mol, bond, atom11, atom22);

					me.doit(action);

					var highlights = {};
					highlights[editor.flash_color] = [bond, atom1, atom2];
					editor.draw(highlights);
				}

			}
		}
		
		ghosts = pt_down = obj_down = null;
	});
}

TME.HandleTemplate = function(editor, template) {
	var me = this;
	me.editor = editor;
	me.sdf = template.toString()
	me.template = TME.parseMol(me.sdf);
	me.pt_down = null;
	
	var old_coords = me.coords = []
	var center = me.template.center();
	var bondLength = me.bondLength = TME.getMiddleOfBondLength(me.template);
	for (var i in me.template.atoms) {
		var atom_i = me.template.atoms[i]
		atom_i.coords.subBy(center)
		atom_i.coords.scaleBy(editor.bondLength/bondLength);
		old_coords.push(atom_i.coords);
	}
	me.sdf = me.template.toString()

	var canvas = editor.canvas;
	canvas.unbind("mousemove");
	canvas.unbind("mouseup");
	canvas.unbind("mousedown");

	canvas.mousedown(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		handler.pt_down = editor.get_mouse_position(evt);
	})
	canvas.mousemove(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		var mol = editor.mol;
		var pt_mouse = editor.get_mouse_position(evt);
		var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
		var old_hit_obj = null;
			
		if (handler.pt_down) {
			old_hit_obj = editor.mol.hit(handler.pt_down, editor.offset, editor.factor);
		}
		
		if (old_hit_obj == null) {
			if (new_hit_obj == null) {
				var ctx = this.getContext("2d");
				var highlight_atoms = handler.merge_atoms(pt_mouse)
				var highlights = {}
				highlights[editor.flash_color] = []
				for (var i in highlight_atoms) {
					highlights[editor.flash_color].push(highlight_atoms[i])
				}
				editor.draw(highlights);
				var backup_style = ctx.strokeStyle;
				ctx.strokeStyle = editor.flash_color;
				TME.drawMolOnContext(ctx, handler.template, pt_mouse, editor.factor);
				ctx.strokeStyle = backup_style;
			}
			else {
				var highlights = {};
				highlights[editor.flash_color] = [new_hit_obj];
				editor.draw(highlights);
			}
		}	
		else {

		}
		handler.restore_coords(handler.template)
	});

	canvas.mouseup(function(evt) {
		var editor = this.editor;
		var handler = editor.handler;
		var mol = editor.mol;
		var pt_mouse = editor.get_mouse_position(evt);
		var new_hit_obj = editor.mol.hit(pt_mouse, editor.offset, editor.factor);
		var old_hit_obj = null;

		var action = null;
		var highlight_objs = []

		if (handler.pt_down) {
			old_hit_obj = editor.mol.hit(handler.pt_down, editor.offset, editor.factor);
		}
		
		if (old_hit_obj == null) {
			if (new_hit_obj == null) {
				var t = handler.template
				var center = editor.canvas_to_world(pt_mouse)
				var highlight_atoms = handler.merge_atoms(pt_mouse)
				for (var i in t.atoms) {
					t.atoms[i].coords = t.atoms[i].coords.add(center);
				}
				var atoms = [];
				var bonds = [];

				for (var i in mol.atoms) {
					atoms.push(mol.atoms[i])
				}
				for (var i in t.atoms) {
					if (typeof(highlight_atoms[i]) == 'undefined') {
						atoms.push(t.atoms[i])
						highlight_objs.push(t.atoms[i])
					}
				}
				for (var i in mol.bonds) {
					bonds.push(mol.bonds[i])
				}
				for (var i in t.bonds) {
					var bond_i = t.bonds[i]
					var i1 = t.indexOf(bond_i.atom1)
					var i2 = t.indexOf(bond_i.atom2)
					if (typeof(highlight_atoms[i1]) != 'undefined') {
						bond_i.atom1 = highlight_atoms[i1]
					}
					if (typeof(highlight_atoms[i2]) != 'undefined') {
						bond_i.atom2 = highlight_atoms[i2]
					}
					if (mol.getBond(bond_i.atom1, bond_i.atom2) == null) {
						bonds.push(bond_i)
						highlight_objs.push(bond_i)
					}
				}
				var mol2 = new TME.Molecule(atoms, bonds)
				action = new TME.ActionNewMolecule(editor, mol2, false)
			}
		}


		if (action != null) {
			editor.doit(action);
			var highlights = {};
			highlights[editor.flash_color] = highlight_objs;
			editor.draw(highlights);
			handler.template = TME.parseMol(handler.sdf)
		}

		handler.pt_down = null;
	});

	this.restore_coords = function(mol) {
		for (var i in mol.atoms) {
			mol.atoms[i].coords = this.coords[i]
		}
	}

	this.merge_atoms = function(pt_center) {
		var me = this;
		var editor = me.editor
		var delta = editor.canvas_to_world(pt_center)
		var t = me.template
		var mol = editor.mol
		var r = me.bondLength * .2
		var result = {}	
		for (var i in t.atoms) {
			var atom_i = t.atoms[i]
			var p = atom_i.coords.add(delta)
			var atom_ii = mol.near_atom(p, r);
			if (atom_ii != null) {
				atom_i.coords = atom_ii.coords.sub(delta)
				result[i] = atom_ii
			}
		}
		return result
	}
}

TME.Editor = function(div) {
	var me = this;
	me.div = div
	me.flash_color = "#B3EDB7";
	me.warning_color = "#FF8986";
	me.id = TME.set_id(div, "tme")

	var width = div.width(), height = div.height();
	if (width == 0) {
		width = 500;
	}
	if (height == 0) {
		height = 300;
	}
	height -= 37*2;
	width -= 90;
	
	var canvas = div.find(".tinymol-canvas");
	canvas.attr('height', height);
	canvas.attr('width', width);

	me.canvas = canvas;

	jQuery.get(TME.RINGS_URL).done(function(val) {
		var mols = val.split("$$$$\n");
		me.templates = {}
		var names = []
		for (var i = 0; i < mols.length; ++i) {
			var mol = TME.parseMol(mols[i]);
			var ii = mols[i].indexOf("> <NAME>")
			if (mol != null && ii > 0) {
				var name = jQuery.trim(mols[i].substring(ii+8));
				me.templates[name] = mol
				names.push(name)
			}
		}
		var canvases = div.find(".tinymol-templates").find("canvas");
		for (var i = 0; i < names.length; ++i) {
			var ci = canvases[i]
			TME.drawMol(ci, me.templates[names[i]], {}, 0.3)
		}
		var buttons = div.find(".tinymol-templates button")
		for (var i = 0; i < names.length; ++i) {
			var button = jQuery(buttons[i])
			button.attr('data-toggle', "tooltip")
			button.attr('data-placement', "left")
			button.attr('title', names[i])
		}
	});

	me.new_doc = function() {
		if (me.mol.atoms.length > 0) {
			var action = new TME.ActionNewMolecule(me, new TME.Molecule([], []));
			me.doit(action);
			me.draw({});
		}
	}

	div.find('.tinymol-new').click(me.new_doc);

	div.find('.tinymol-save').click(function() {
		TME.download(TME.SERVICES_URL + "download-mol", 
			{ mol: me.mol.toString() }
		);
	});

	div.find('.tinymol-undo').click(function() {
		me.undo();
	});
	div.find('.tinymol-redo').click(function() {
		me.redo();
	});

	div.find('.tinymol-cut').click(function() {
		alert('TODO');
	});
	div.find('.tinymol-copy').click(function() {
		alert('TODO');
	});
	div.find('.tinymol-paste').click(function() {
		alert('TODO');
	});

	div.find('.tinymol-zoom-in').click(function() {
		if (me.factor < 500) {
			me.factor *= 1.1;
			me.draw({});
		}
	});

	div.find('.tinymol-zoom-out').click(function() {
		if (me.factor > 5) {
			me.factor /= 1.1;
			me.draw({});
		}
	});

	div.find('.tinymol-reset').click(function() {
		me.zoom0();
		me.draw({});
	});

	div.find('.tinymol-check').click(function() {
		alert ('TODO');
	});
	div.find('.tinymol-refine').click(function() {
		me.refine();
	});

	div.find('.elements button').click(function() {
		var symbol = jQuery(this).text();
		me.handler = new TME.HandleEditAtom(me, symbol);
	});

	div.find('.tinymol-templates button').click(function() {
		var template = jQuery(this).attr("title");
		if (typeof(template) != "undefined") {
			template = me.templates[template]
			if (typeof(template) != "undefined") {
				me.handler = new TME.HandleTemplate(me, template);
			}
		}
	});

	div.find('.tinymol-pencil').click(function() {
		me.handler = new TME.HandleEditBond(me, TME.SINGLE_BOND, 0);
	});

	div.find('.tinymol-single-bond').click(function() {
		me.handler = new TME.HandleEditBond(me, TME.SINGLE_BOND, 0);
	});

	div.find('.tinymol-double-bond').click(function() {
		me.handler = new TME.HandleEditBond(me, TME.DOUBLE_BOND, 0);
	});

	div.find('.tinymol-triple-bond').click(function() {
		me.handler = new TME.HandleEditBond(me, TME.TRIPLE_BOND, 0);
	});

	div.find('.tinymol-aromatic-bond').click(function() {
		me.handler = new TME.HandleEditBond(me, TME.AROMATIC_BOND, 0);
	});

	div.find('.tinymol-positive-charge').click(function() {
		me.handler = new TME.HandleEditCharge(me, 1);
	});

	div.find('.tinymol-negitive-charge').click(function() {
		me.handler = new TME.HandleEditCharge(me, -1);
	});


	div.find('.tinymol-eraser').click(function() {
		me.handler = new TME.HandleEraser(me);
	});

	div.find('.tinymol-select').mouseenter(function(evt) {
		var data = {
	        display: "block",
	        "float":"none",
	        left: 50,
	        top:  100
	    }
		me.div.find('.tinymol-toolbar-pickers').css(data);
	    return false;
	});

	div.find('.tinymol-toolbar-pickers').mouseleave(function(evt) {
		jQuery(this).hide();
	});

	div.find('.tinymol-periodic-table').click(function() {
		div.find('.dlg-periodic-table').modal()
	});

	div.find('.dlg-periodic-table td a').click(function() {
		var dlg = jQuery('.dlg-periodic-table')
		var symbol = jQuery(this).html()
		me.handler = new TME.HandleEditAtom(me, symbol);
		dlg.modal('hide')
		return false
	});

	me.draw = function(flash_objs) {
		var me = this;
		var canvas = me.canvas[0];
		
		canvas.editor = me;
		var ctx = canvas.getContext("2d");

		ctx.fillStyle = ctx.strokeStyle = "#000000";
		ctx.lineCap = "round";

		ctx.clearRect(0, 0, canvas.width, canvas.height);
		for (var color in flash_objs) {
			TME.flashOnContext(ctx, flash_objs[color], me.offset, me.factor, color)
		}
		TME.drawMolOnContext(ctx, me.mol, me.offset, me.factor);
	}

	me.zoom0 = function() {
		var me = this;
		if (me.mol.atoms.length == 0) {
			me.factor = 40;
			me.offset = new TME.XY(0, 0);
		}
		else {
			var canvas = me.canvas[0];
			var w = canvas.width, h = canvas.height;

			var size = me.mol.size();
			var wfactor = (w-10) / (size.x+1);
			var hfactor = (h-10) / (size.y+1);
			var factor = 0
			if (wfactor > hfactor) {
				factor = hfactor;
			}
			else {
				factor = wfactor;
			}
			var center = new TME.XY(w/2, h/2);
			var offset = me.mol.center();
			offset.x *= -factor;
			offset.y *= factor;
			offset.addBy(center);
			// offset.y -= factor*me.bondLength/2;

			me.factor = factor;
			me.offset = offset;
		}
	}

	me.load = function(url) {
		jQuery.ajax({url: url, context: this}).done(function(data){
			me.mol = TME.parseMol(data);
			me.bondLength = TME.getMiddleOfBondLength(me.mol);
			me.zoom0();	
			me.actions = [];
			me.redoers = [];
			me.__update_undoredo_status();
			me.draw({});
	    });
	}

	me.doit = function(action) {
		me.actions.push(action);
		if (me.redoers.length > 0) {
			me.redoers = []
		}
		action.redo();
		me.__update_undoredo_status();
	}
	me.undo = function() {
		if (me.actions.length > 0) {
			var action = me.actions.pop();
			action.undo();
			me.draw({});
			me.redoers.push(action);
		}
		me.__update_undoredo_status();
	}
	me.redo = function() {
		if (me.redoers.length > 0) {
			var action = me.redoers.pop();
			action.redo();
			me.draw({});
			me.actions.push(action);
		}
		me.__update_undoredo_status();
	}

	me.refine = function() {
		var data = me.mol.toString();
		jQuery.ajax({url: TME.SERVICES_URL + 'refine-mol/', 
			data: {moldata: data},
			dataType: 'json',
			type: 'post'
		}).done(function(val) {
			if (val.success) {
				var mol = TME.parseMol(val.result.mol)
				var f = me.bondLength / TME.getMiddleOfBondLength(mol);
				for (var i in mol.atoms) {
					mol.atoms[i].coords.scaleBy(f);
				}
				var action = new TME.ActionNewMolecule(me, mol);
				me.doit(action);
				me.draw({});
			}
			else {
				alert (val.error);
			}
		});
	}

	me.__update_undoredo_status = function() {
		if (me.actions.length == 0) {
			me.div.find('.tinymol-undo').attr("disabled", "disabled");
		}
		else {
			me.div.find('.tinymol-undo').attr("disabled", null);
		}

		
		if (me.redoers.length == 0) {
			me.div.find('.tinymol-redo').attr("disabled", "disabled");
		}
		else {
			me.div.find('.tinymol-redo').attr("disabled", null);
		}
	}

	me.get_mouse_position = function(evt) {
		var offset_c = this.canvas.offset();
		return new TME.XY(evt.pageX - offset_c.left, evt.pageY - offset_c.top);
	}

	me.canvas_to_world = function(pt) {
		var me = this;
		pt = pt.sub(me.offset);
		pt.x /= me.factor;
		pt.y /= -me.factor;
		return pt
	}

	me.atoms_in_rectangle = function(pt1, pt2) {
		var me = this;
		pt1 = me.canvas_to_world(pt1);
		pt2 = me.canvas_to_world(pt2);

		if (pt1.x > pt2.x) {
			var x = pt2.x;
			pt2.x = pt1.x;
			pt1.x = x;
		}
		if (pt1.y > pt2.y) {
			var y = pt2.y;
			pt2.y = pt1.y;
			pt1.y = y;
		}

		var result = [];
		for (var i in me.mol.atoms) {
			var atom = me.mol.atoms[i];
			var coords = atom.coords;
			if (pt1.x <= coords.x && coords.x < pt2.x && pt1.y <= coords.y && coords.y <= pt2.y) {
				result.push(atom);
			}
		}
		return result;
	}

	me.mol = new TME.Molecule([], []);
	me.offset = new TME.XY(0, 0);
	me.factor = 40;
	me.bondLength = 1.0;
	me.handler = new TME.HandleEditBond(me, TME.SINGLE_BOND, 0);
	me.actions = [];
	me.redoers = [];
	me.__update_undoredo_status();
}
