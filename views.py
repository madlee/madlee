from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

'''
Created on Jul 25, 2014

@author: lifej
'''
import traceback
from csv import reader as csv_reader
from io import StringIO, BytesIO
from base64 import b64decode
from random import randint, choice
from hashlib import md5
from time import time as time_stamp
from PIL import Image, ImageDraw, ImageFont

from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.views.decorators.cache import never_cache
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm

from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route, permission_classes
from rest_framework.permissions import AllowAny

from madlee.tools import dump_json_string, extname
from madlee.serializers import UserSerializer, GroupSerializer, uniform_object

class ResponseError(RuntimeError):
    def __init__(self, code, message, errors=None):
        self.__code = code
        self.__errors = errors
        super(ResponseError, self).__init__(message)

    def rest_response(self):
        result = {'detail': str(self), 'success': False}
        if self.__errors:
            result['errors'] = self.__errors
        return Response(result, status=self.__code)

    def raw_response(self):
        result = {'detail': str(self), 'success': False}
        if self.__errors:
            result['errors'] = errors
        result = HttpResponse(dump_json_string(result), status=self.__code, content_type="application/json")
        return result


class ResponseBadRequest(ResponseError):
    def __init__(self, message='', errors=None):
        super(ResponseBadRequest, self).__init__(status.HTTP_400_BAD_REQUEST, message, errors)

class ResponseUnauthorized(ResponseError):
    def __init__(self, message='', errors=None):
        super(ResponseUnauthorized, self).__init__(status.HTTP_401_UNAUTHORIZED, message, errors)

class ResponseForbidden(ResponseError):
    def __init__(self, message='', errors=None):
        super(ResponseForbidden, self).__init__(status.HTTP_403_FORBIDDEN, message, errors)

class ResponseNotFound(ResponseError):
    def __init__(self, message='', errors=None):
        super(ResponseNotFound, self).__init__(status.HTTP_404_NOT_FOUND, message, errors)

class ResponseNotAllowed(ResponseError):
    def __init__(self, message='', errors=None):
        super(ResponseNotAllowed, self).__init__(status.HTTP_405_METHOD_NOT_ALLOWED, message, errors)

class ResponseServerError(ResponseError):
    def __init__(self, message='', errors=None):
        super(ResponseServerError, self).__init__(status.HTTP_500_INTERNAL_SERVER_ERROR, message, errors)

class ResponseNotImplemented(ResponseError):
    def __init__(self, message='Not Implemented Yet', errors=None):
        super(ResponseNotImplemented, self).__init__(status.HTTP_501_NOT_IMPLEMENTED, message, errors)

def validate_form(form, message="Invalid Input"):
    if form.is_valid():
        return True
    else:
        result = {}
        for field in form:
            if field.errors:
                result[field.html_name] = field.errors
        raise ResponseBadRequest(message, result)

def rest_view(func):
    def new_func(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            if 'success' not in result:
                result['success'] = True
            return Response(result)
        except ResponseError as e:
            if settings.DEBUG:
                traceback.print_exc()
            return e.rest_response()
        except Exception as e:
            if settings.DEBUG:
                traceback.print_exc()
            e =  ResponseServerError(str(e))
            return e.rest_response()
    return new_func

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    
    @list_route(methods=['POST'], permission_classes=(AllowAny, ))
    @rest_view
    def login(self, request):
        form = AuthenticationForm(request, data=request.POST)
        validate_form(form)
        user = form.get_user()
        login(request, user)
        return {'user': UserSerializer(user).data}

    @list_route(methods=['POST'])
    @rest_view
    def logout(self, request):
        username = request.user.username
        logout(request)
        return {'username': username}

    @list_route(methods=['POST'])
    @rest_view
    def set_password(self, request):
        form = PasswordChangeForm(user=request.user, data=request.POST)
        validate_form(form)
        form.save()
        update_session_auth_hash(request, form.user)
        return {'message': 'Password Changed'}

    @list_route(methods=['GET'], permission_classes=(AllowAny, ))
    @rest_view
    def who_am_i(self, request):
        ensure_csrf_cookie(request)
        never_cache(request)
    
        if request.user.is_anonymous():
            raise ResponseNotFound('Not Login')
            
        serializer = UserSerializer(request.user)
        return serializer.data

    @list_route(methods=['POST'])
    @rest_view
    def reset_password(self, request):
        raise ResponseNotImplemented()

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

def ajax_view(func):
    def new_func(request, *args, **kwargs):
        try:
            result = func(request, *args, **kwargs)
            if isinstance(result, ResponseError):
                return result.raw_response()
            result = uniform_object(result)
            return HttpResponse(dump_json_string(result), content_type="application/json")
        except ResponseError as e:
            if settings.DEBUG:
                traceback.print_exc()
            return e.raw_response()
        except Exception as e:
            if settings.DEBUG:
                traceback.print_exc()
            return HttpResponse(dump_json_string({'detail': str(e)}), status=500, content_type="application/json")
    return new_func


def captcha(request):
    BACKGROUND= """iVBORw0KGgoAAAANSUhEUgAAAHsAAAAiCAYAAAB/RqZAAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAB96SURBVHhepZvbk2XXXd/XuV/7nNP3nvtoNBpMIB6rHMDIWC5jYwMCUXlIQsIDcUEqFUMqFEXe7FQ/uJJU3nlIFSk/hn8glSiBsoVlTMWyZAUBkpFnRjPT0/d79+k+lz4nn89v91Fk2UmFZo+29tl7r73W7/f93ddanfux331uPDU1lRqNRtre3k41rpVKJT1aeZwq9VrK5XLpbDRKZ+NRyufzKVcsxPXs7CzO/DiX4hiN4z4Nz9J4PE75VIhv87lissmIkzecuTTK85w+Srl8mi5X06c/8cn00ksvpcFgkIa9Yep0Omn/6DA1m810cHicpmdn0s7+Xqo2G2ljcyt99oVfSG+88UY66h5nY3PkximjjzHff0jLCPonZHo064109+7d9N//y39NC/Nz6fToOM20O2l3eye1phrp6OgotZtTaW9vLxUrxVQqldLnPve59Mdffznt9k/TACzsMw/PBXiCU3DIaBiNh9mYciot4FUoFCAuI2BEI+89gy7w8loAiwL0+22ve5KuXbmaer1eOjk+TrOzs+mY6+HhYfRxUXkVPvz3f2LZBg4ypFGtVgtg7LhSrcIKgJ0DpcAmhx1IMN3FAONzYZ+djXiSIPycST7OKVx+5wq05jkfnwtllLr7h+neO++khcUF2thnMe0CchXCe/1eKpUr6fj0BB06S8VqmS/Qp9FZWttYT8VyGSwzZvw2T59Zv9nh7zh9ZxvOMfdHKNKI/k66Xfovpd7JaeqjaGX66yFMn+0fHKSpdivVm/XUarfTt1/7djroHqVUhn4EY1/F81NBKWjByhcyLEdjsOAKARlO53QWAF+aJko44hoY8i5/LvBBv5+mWlPBW/DN1T7a0KEBTL7/m8orP6Cz3f39tLWzE40EWa2ZX1pMS5cuRacT0GKA4TDAVlOLgJIrKjgtCC2NU62GR2VaFOiQbXwfzAKG9wo62tLPwtWltLK+lv7s1W+l+Uvz6ff/4++nn/+lF1JZTS3n0xArKdWrYVF5LG11az31z2lwLAVop3mAlKZKrRqnv33mO9vY1m/81j7syz7t2zEcyzEdWxqkRZqkTRqlVZqlXR4yXjLF9f79PAddtJ3gIoliJU3SIIZiaZv34yvmYq8MlIUyUTbKSFn9beRVuPPC3WVvZCE0EKp7wwEa3k+nWFb35CQ0xI4hO9p52NZTC4nnMoam2cCB+F+mrfwL943lZ/8yIDz8ncP/dU+66QA3deOpm+kIK375la+nP3nlG4D+i+l79++lGu47h9UN+VJmvS9wLwOOqefwn2NqKe8/J/QG/dzrVstYQKWiFfdToVTkvpKqeI0BwHzyZz+V/vMf/mF69TuvpT08wDweZ/fwIPXP+gibfs6F65Gzf61TvjFtPYtY6EbFwyM8jgqnV+O9Fq4HVAjRQpzOvxNLn/k+rrQxtNlGq20i1GKxGMLz/d9YXnPPXVkO6wQwyE69QY/OJGSU9g72AAitkLdgVAHxD8Y8bT86G/IQbbJ3BwmAGQICHUhmz+hjeH76XQ6L4HX02ZhqBqG37jydDo8PAXWQNrY303UE/+Zbf4FwcqlUq6AMR9H2dISLm+3QD5qKoM6IeXmFzf0ZICgw+xsAQAiXcaQhmKdNuVGN76rNauqedgNAx63yXIH+9b130uLlpfRw5SEKVQSlXLrx9M20e7CfGu0plG2Q0S7/8H7GKRI5hQ0dCiJTskwJHUuBRHv+aRriO8LaQz/FQsT8juenvZNQxCHjnPZOA69CCaEiox6GYJi5qLwKpQ+3l4MW4xDADo25KGKD2HB8ekxT0aJxaLQuQuDU3Eyw9B7CDj3y5/k/b0IgajKMndkEAk12CminVu9hkjWA0dNBH/BPwmo9S2jyIdauW+0B6A5xfO7KUvQ5pp8e2l/FVXdJ4DLazQnUdMbkt2N4zQO242ptA/ppdKbSKYrAJ9HGsVafrDJeOQSjotRRqmIFOnG5h9DX1WIA3nelIs+1EugHkRCafKsACtUxFFzovm3ETfzg1f9PPI6Y5gFaTANjhUSPymB6eoYc4jTyn2Ipj1KNUx8h7+7tp2O8oNBdRF759nSLLI5Eh8Sj1W6SpMAw2jVAYxygXCXuMaDCVC/tyM49IwXlaaKzIVmoluHp73Ge9gzmV1pHif51lTWs1Kv3EjtHXLx861rK1Yifefqv5NOtH72dNnY3UwXrG2tc1SJCaqIUg1SdQvhYt+8GjDNkbK2jCN1l+i3Tv4Lz9LfPfGcb2/qN39qHfdmnfTuGY/nOsaVBWqRJ2qRRWqX5h/Eij/Iqz/L+QTzEKLD6AH6BKf8XY7EWc7FXBspCmSgbZaSs/jbyKsw/f2X5BD+v25iZmclcHmrQmZ0OTdcCFGm4HuMjTKmtZp3oEH2plVkmbh9qo0doNe4GM4jfRfqKWIk1at1qt269D0EPHr6bPv1zn0lPVtdwS7rgYbiuLlmyMa4zM01m3A5XXscSjdtzC3Npa2s7laC3XMmSMWOVWfUJVtinD00gDxgT65bxHla3dHkxdSlvatBiFruwsIDQyPqPu8S9fmq2G2FB+7wrUdb89M88l175028E7cbpSqmcqowpPmHSwBpWKxSCDE7ikcXyrASaxFdBD+z0ULbVMoGpDCYVvEmLCqDTaadhf5CalIGWVVuEtdNT3LeywJNcVF6FmY9fWi4IGAnPAKCHuiE6NNDbkfVdZ6ZDJ7hO6j5do4w4eIVvRoA65DtjV8YqJ0IsQ3gdYhWeWvGlf/Ol9Oprr6XH1IPTCG9zczPNzM1GNtyeaafHTx6rI+GaVQzd45C+T4ir9t3XzSOAnd1t3BJubYhlArjlSpn2kRsgyD4u17jm70Jkv0X6A2iYNUmq1iuEhwOEfZy2oSFcLmcXRTriLNFebxAlkkDyb3VjFYtqw0YutajRd7a2QigPHz0MXn7vX/9u+urXvhq5Sb1Rg+5B0Bsx+1xAkZELOLQcM3aEG16alFk3TxE6jlCuUDrqfFVBHk6Rg4ffTxToovLK/fiXnhtHxsdhB1pdxJssCKX1zQ2EMU2HAEw767o+zOyT/huz5jozFP5dQO5jyZQ+xMH+EHdJW5OVeq0Zllav1sKCTk56QXAVKz8Znqa1vW1AyDL7Mt9WyrVQIsezj7mZOcDpRl+FUiWUSQt34kNlaDfbCJz4fN6HsfT9R0wwwGwAhTvdP9oPGq1XW41m8HCGCyxS/zfqdUqarcBA/o3Rvf5JeAm/H3MudWZTrVgN8LQYXfnGxkaWb6B83ZOj6Ms+yiibNI5IIsW21qD/vZ3gwZq5TPzXSrVqx9vf2U2L8ws4CpUD94uJioHYetinx0XlVbj1i3eWtUw13UmMLLs0sNNBuAC1M4+FGRcoVbj3nVbs70q5EuWH1mZbXZkaGS4dTZeICnWsLlOL3dreBpDj0Ghnd5otY1AF4Bupw+86gJcQWhEtraEQhjrHHkK4XuSQrFhL/Oizz6Z/+A/+UXr9jf/FeCiGAtF6/MeY4coYw8RPZgVEzf/tL/wLwBymtSdPUg8lMnSOeC8/FZUNj+TYCqBuDQvtPtPaW/WpmPE6QdEOjg4CLy05/Bk6NuB+QIwO3OlD61L1vJboWysTFz1Nid/ycwpvI/hxbJ/5zud6KCd9lIvy0eKnCGFlwtVF5ZX7xH/47NipSCWvxqj1ZeKEoBuj1N4+HeomvPpeCwrNoxenN51BMiYKrFYo6BW0vIomd49Pw5Jv3LgR03udqU5YiVZgkhSTJTCrNVQZ15yiSx9jYplae3x0EvfS4rThgL5W19ayMNKsE19n0yn9DXvUwbwLtqDNI7SdQx6KlDNV6D7a3069o254lktLS6nEO+lyFk2eG02SNnDQhXuvNpziDvUsJWgcdBk3Xw5vJQ57h3tB17vvvhvj1CnhTvF0Pd+Di96iSozHrQQuTr9OsBMHLb9cKOIBa3GVB2mR577j8l4c9ARO6XpcVF6F5kemlq3f1CbkFZqoZYYPQRuOcZnWciWsUu1SU6wvtS41WYubJA7Gj5guZUCTIt262tdAKPvUgNPEvfZUKz15spIuXVoEiAbP9yOLzMFkEQByhIAzQwJja+EN3H+B5wMSllMAMHFRKVq44VqtTsyHXqsCaNe7eI0TmrSoiJPQEVrP6ym+nQLYCgD2BfXwKNxmC8VsEzdzeKMxfPhMekZk6z2AU3hjaFuYmQWLQuQeS0uLjDnCda6Ta1SCV2N+4Idw9TIxyyZt9KuljQkl3ut7zDWqKKFJ3wn5iHIQR7PrPuPleT5pK959FPxEi76gvAr5D5Vw42gAWlXxJQBLqC4wkgEE1zO4R1cpftu5GuNRKhJfudo23DLCtHbVEsxmzcBNxMxgS8SYzbWNdOvWrUhC5onhRyRLCtVximiN/xANjHDPddAbhIK0ptpBV5cExKRlgNYfcSXwRfESBGrJnDEPjWX6O+p5rmHtMH28u5fOTilTOGEkFjzmZ+dSDdpUJue6tbrJnDsoCfV7oWVxfpEqYCumJh89egSvhB0UR55PwOYQvgxZWqJhSOGF4OkrYjNdirFxWMWVbL2S9OjqteYB92JgvxGTofP03NojE7+gvArXP3VjORgTKJrElJ9uxpb8Xrq0hMaqkbBME12HHUY8pYkxRKuJb9Uma1u01Tiq+5ue7kRckkCJnZ+dST//C59Ljx89DhdlzIkwYFIHwy4CoIpYN+0R7NzsvGYBML1I4JYWl3C/lyLh0/0P0dw+TBqzJVnmY9FFQXEIguN4aKlNkqKrC0vp+tVraZocIUcfxmwXbuRpa2szLE1LNJtVINKnIAw1hOcY49Of+RTCfhhz1LpjrUkhK4wpvE6DeK9Vmc+E4KiLtWSRcmJGTyVdKpRtFOrC/Hw63D+ApszNx3jQomczaTUkSONF5VX46D/+qWUbKRi1LSxUYCCsCAGLWJ/uybjg6o/1rKtCQzrbOziI2JcHiLzxhmeZS9G60GSSmjIJnDHNhOI2Fn3t+rX06re+FX2abLVwxWWKz8IZaQ6glc7jl6tf0rK6vkq/Z7g7nxXCjR0QEnT1Fdodw4xl2JnxC6bq0KNrVDEUd4Qa2obbZJxZQscZAjkiwfKbsBzo3t3fSWuMZezztA8ICMHX8V7GXcHfpvRzuuLeve+lW888Ha5yjxi6sLiYxV9isiFCAaq4IVwEZSjRynfBTG/nd3VCRwuvpcBUppnONLwhbL7RmlUiT+UwTU2tN2l3iNsXlFfhxgt3lsskSVNoeaM1FUBLnIdZrJp3cHQYy4xqSp4PB4CPVNKMa8EM6gR8lT4iRlJuWAeaTdbRdDVyY30tXYFQ49Lu7g5GPEytTiut3LufrrTQ5rWt1EYIM61O5AAmIC46YPfpjJh40D1M125cj1hlfK8g9HmYPyGrb0B3ESC7Kh7M9vEeZvYGmX2szgRsCI0LKNwsFlcgFi5Mz4QSOM+tEBrTrXT/8YNUqjllSjaMUI4JQVMo2GJ7JhUGZ+lwYztNQeO7Kw/SpRtXSYC6aW9vN2J5GxpWiOFt8DN6GG+N3Wqbrj9qa9r7cnZpITyShuF0rRn3AWNFdYJ1WlKKWZP8od1ppw4h0PV8k1ErHc+Lyquw9LPXlx3AQ+1pUuAbN6KOhYCrWGJmsU4F4jKNe7plieNeLYtOBZznZslqW8QWNL0FQfMQHOUWTAqCkw6uKX/o6Ttp897jNNdoYeUHwfy+a8aA0kWoRULC0ISFxOYSyZDaq5COyETtyxRI5h3rhETLBGZkVs64xt9Tn5noYCVaapV+85ROPWg5ODxIFTLnJkpnGHqysR7ZbIl2CqCCqz3D1VaQ2OH2burUmlE23v67H0pPNlcj9sqLSZehaHFhPpKgHRRM96+3EIvIwFHCiXDFzYpF69NFuw9Aj2Re4ozY5oZhJIFbK5Ytpck84IiQ5iGmF5VX4dl/8pPLukcFJ4FqmC/wLeHiHpKEuJnAkNBEcK6dmogpUM9KqcrV78Zpf+8Ay3RGCK1rNNN0ZyZ94hPPp9df+06amZ6FMWO8kxCb6fbtZ9LGk9V0c/EqyVEt/flf/mVqQfwWgjQGm3w1cUOXrl6NuGiYqGFpJoAmhd5nZYV1J26eeyJZmscVWtbVSmbdlE7Q1oYWE8Qm7lNLzyPIOiC1sBhziy34c7zbz9yJRGgX4Vrf90+Iw/B3/5176Rnobc900srOerp8/Trx+nHwB2S4/UZaX99IP/eZz1LG7UCXkyRY2P4hgkBhyng94z40xKoVWBt/9RToajpC0d0lYwnYbrXJe6gWDI96JYQV7Z2HwIUbYi4qr8L885eXnX917XRlZSU0SxCNxeahTz31VGimqzk9XGzMKqGxMQCHiZcdOVDUzxBqjJ7EPhlQw99+++24d9Zneno6PX78GPAb6f5ffTcdoCTXnrqZPv7J59Orb7yORVfT7R/9kbSJy/+pj30spgDN7hkpEg5Dhwy5KiaIPWpf46mWqIWY4JgUKWBr3ioK5jvz0x1cbxd6cngJvUb3rB9uVav68N276a2330Kwt1Hcvfj2xRd+KSz90cNH6dHaSpqan00rG6vp8uXLwa8W9d3vfjfNzc1FAip2ntl0psuhucDS+Oq9OY2hSnqM6T2SUmk3Obt540Zk9DWSTzFTHk7hOp2qqzae68UuKq/C4JkRwi5HfI0yAR9SI5P0NNPWXbru6/pwmZjmHK7zwm6bMblYQ6O1sjYWVcWSamirycHh0XG4nh5MOW13DWtYW19PT2Mhap+JhnXvpem59OKLv5IeUXv/0ctfSwNIdv36Ppmu7vydd++nVVys8UgwdWlqvgsiVwD85BD3ppLBg+67Ciju2zL5MlHy2qA9JhCgXL2Jm8Ntqzjr25uxhHlIPF0nC3+XMS2BHj18GP1Y4r3552+mSyRfv/ziL6e9w/10WqAMA5vtnd3g6d79++mZO3cIU9nWpv0DrBkDqCAwQa+p4Hga8TCU3cDNNom/U1ic2blZ/xi6nT3zXsxdVzApNXdowmeDRG6fsR+AhTnPReVVmP74wrIWWsYK1CABUXt0EbqBbUsLfjuvq9WY2Z10T6LWXEerOiQ7eTRTbdPC/c5DTVa7dTlqtKfa/+DBg7QIeGri8R7JSCqlP/vmN9PjtdVUBYT17a00TYwv81srdE3Z2S/nz99bk4YBa/gulucESY02dRStQgyVjzwACJwWUqMi8F2ZBMXMdH1vJx0ARpFseJeM3PVx3ZwrRm6EMHfQM5nLr6+upVn4WyPcvP3WW6lKODjEDTdJ6KT/3r176RKJp9Yt3/KrJRtXwwVz9Zg8szRzQqkPvlYduvI6imDlopcQM43ABRHjvnHb099OPtlGPD0uIq/C3PNLscQZiw0AqRtxs51W6eBOy6mxaortfC7h1pLWvPX6FPcjAHYFxzieJR2e7XYnvfnmX0Ss2qd+fOc7b6S7f+8n4vcRyZOzYPnhOGrLMdamsFff+quUJwvdI4G6ded27BBRwK4Uael1LOISFq2ly+iAuFpAw6XV2Srdn1fwjWdFqgOfaUFdkqkc2p7DcqwkYmMCCZyuEopDWW9grY9x2WdY5+H3EObNp8JjmDSZ/eYblbSNix+iGNb73/wff2SmZoRID/nu+nVcMVaebcDE8njuTJrhxtJqdroddXNgiZK4iCR95gp6EmnewXpjlwpCdY7bxND2E+O5qLwKrY/NLquFk9OArsaoKe4OmbFk4ZwDHOOxMcBO7EAjLhIPe2TAkxg9IcZtuLbxW/ubJya1OY07EuThosLxLiUObsa9ZznLg+lO+ue/9YX0zoP76R5eIKtRs/lk6dOaBcGY6rbfHNpujWp2q9s8JMvXSo1flkdms74zy9Z156iDN5wLBwy1fTL5Ys5hsvNk5UnMmf/G5z+f3nrwbqxMyccMyZzzCgMytxMrAgQluIvXrqUl2isI+ZzwZ3+urImJlq116ma7KPExNBqv9Q4Lc/ORL+iFnAxSsC36ETf78Dst2kMMJjhcRF65xd+6PtYF6y78WIvy8F6gTY78SMIkWgYnpcUY910iyRrnsmzPd161OJn36vcuEnh/lcz684D45S9/ORh0kqOMpzvGyn+G5Ox/vv7t2NzXda261YzSy+RJTc2Ui3hJn9LrJEd8P8ZqUJpJCJmA4OEzT+n22Rh33cdE+1i5izby4cyb74NmwHWxY4i3OD04ismUaVzhTz770fTKy38S4aNP+ez3llhf/OIX01e+8pVINuXdxR7Bl1bvvTquvHt13n1wSlIJT44pH76Tbq1bofi9iqDSeHQYXx68l16Pi8or9+PLHx37wBcSp2tT29VIG6idWqsdO+gEdKfgcgCey2sx+ExcljXdCPdnluu9lbDXEvHxmMRFi1iYnQsCBdUSKZ2SpeIZOsRzN8wdIwS3+NbQ1iNiq7NzsXkhtJu4SKnlJH8ZQRQpN5x5c7ZpkhdoBRNLkG5PLcTTlawzkzNcrLNuA2h1g6OOthxt8Qjw16zWSfwOqdnJD+Cx0+qkPbyA3oHMLVa8VEIFsUGOUcO76eKd7jXUWAl4jbl+aaXfuIf+8aifxtbm50qokk2wlW49hs9NRp2FszyzjZirGB4XlVfhmRd/ZNk0Xv8vmALiBLtJgzNjzjAZM50IsU2sXSNMV6kkzs0I7so4431MdPDMtmbBXtsI7Z0//tM0dWU+LSzMxcyUU5UzuOu11Scxw1TBvVlKOe1lKVIoSUOVxMIdG24WhDaEmsMNmxvkcKXGUFenjG2OnU3T8h9G7Q4Vz5jR497MVtp7CBjiM2BHA74XfFQV3aRVynMdwYv1/ICwMkAR3fBnTlEj1p8OejFdOjM7HVmxkyHzlGLO7D3+xmvp8oduxaqTkyvRn4iClVg4ozjAenPBS5Zpi6V0GV81FHkwRuvunUBSQNnCDNaMoJ2pc8XvovIqTD83GwmacVALdNO5c87GP4WphoXFMIBHYZxN7ofbIX7OuqKFZls6gGIwbunjAruJSGxCWKAsg0i3AU3a0QkaS4aMQGqNWppyQ6FMD7INd+7IdCvRkDrYsgnJAJ5LfjCIwMYKGGZOUY6YnUKqZu56izJx2vLPjNcZNmOxMTosAiGMtGbGx4zf6zsn+Fyl7wzQ3KAAzrGAMDubbfNxtsxtTrF9Gh5ckNjDnbupoL5IVQK/zuI5a+d8fCxAQEcNXONKYrgLBhqF9KqwUQ/jZp078KqLdqeoyq7QtVCTRWtzLVQeLiqvQvkjtWUH0S2oJRbfduS9U31OKNiJgDmYExbhHujQ2GJJYKngHi4TDxc3nNK0Dpwjqdna3EhXr1yOd//sN38jvfJ1Yh8EHuHKm5QHctxHuNepPx+tPIJZV8xgjr7jN0ISWP+uKtZ3gwYEBQAK2y0/ITjO2MdNe8d3CdSrSpc9h3l4MG7m45r1Mzn93me6cwWtNSkkV+yevvVUWqU0zHacFNLB/l7wIE+/TTL5nddfi78ZW6eNV4XtOxVfGmLxxg0HlH3W1GLrAOI6UUJxF1eFpxLpfsVXQRnmJpm6QruovArzzy/GqpdxwVhrgDeDQ02jsT159WM1zCsPQivVXJsIgttqXNp0XbjVnkrXr1xJN5+6mdafPEmbW5uxSOHfS/3LL3whfe3lr6Zf+9VfTf/087+eXnrpv5G97mHZrXT/3vfSzNxMWHzs9MCyGZCxEDYDadkKROEbLhQiLYIWzDueS6PguDQZtEZbrZhvsWqtOQQdfSng85N7+1foKtpkC9D66mrQ9Pjhw8gV/v2/+7ckbVPp1W+/mn7vd34n/cF/+oNYdFnfWE91eP/Is3dTWZcAUfIsJlbGhhInkYzfWWjK6mqTTz2Tmbh5hVfdb9TaCFb3HF4AGZlAGo8vKq/c3/nSXSqgrFjvAYRbay3KLVniI8DSegRXwCRYITN8EOACiASrgRLtpLz3/qVFZ3o67e3uRpaoZvre2CNxs2Tjzqi5IR9xpDqu6/GjR+kySuIhE7E1ljGkwbE9DA0esQToM8BTTKHp5+f7D3mYnCFsgIx9WYDoEX9858EzXabtBMms2OPJykq6SnnV1a3TgztblhYXY/Jiwovu21k9LXHCs/WtwjGc+H6yeSGwZAx5zhYrsrHjDxzAUhrkwa1J8mpZVcmbpJ6FxXtcVF6F9k/PLEfMQ4vlf7JR0LiidekGJU19NL5G0uHaM4BoPRUSF5cKeR0bF2pu0CNjNWnw3ilClzzVUDfnHR0fxh5nlwcblFcud84vLcR0Yr6EOCAu24aLRp8zFwc0ZgLLDmlWi4GJCz/0xNA9uZqU8B9X42t8EYyff/SeUthPKMk5yCFw/qmwWp+b8XlJ3Tobrtk16ENCUJsEM654pOgPYLTeKXjSjMTAtm4gjASTPp3VOznshmEwGJggCL0X7/xDAnMCE6uMxqy96/P+5evEU4WLvqC8cpf/1fWxNZoTBm619U9GLC10WXasRngY6HUF+TFPOAVOrRwW6ShLZ7/voHkcP/gme2dfXseiFA/p1069ngticijk8XmHCsNDWUPwOU0+8bmjTa4e3/9MeqMbLel8iPAaHNmfyvzguAQ9hMPzcx4zt5/14/WDR0bd/+UdvBWHCIB3GS3SRP/8Pjv/QItXyS0LTTbl263S7ryd1N4XldeEtnNmMoZ8qMvzpFqL0ziXxUw7yBjO2ksswvjAGX/6429Gef858uq7yXXSi4xPrnH6+/9x0jYE997hk/dfPX7wmd9490P7/L7znI5oe36N7z/AwwfO9/j+IWfWQ4bdBMNJHvIezufn/xEM3zD+DxzxLHv+/yevlP43GVmPmnOmq4MAAAAASUVORK5CYII="""
    BACKGROUND = BytesIO(b64decode(BACKGROUND))

    img = Image.open(BACKGROUND)
    size = img.size
    draw = ImageDraw.Draw( img )
    font = ImageFont.truetype('David.ttf',size[1] - 6)

    code = []
    x = 3
    for _ in range(3):
        c = choice(settings.VALIDATE_CODE_CHARACTERS)
        code.append(c)
        draw.text((x+1, 3+1), c, font=font, fill=(255, 255, 255))
        draw.text((x, 3), c, font=font, fill=(0, 150, 0))
        x += size[1]

    response = HttpResponse(content_type="image/png")

    salt = "%05x" % randint(10000, 99999)
    code.append(salt)
    code.append('tinymol')

    md5code = md5("".join(code)).hexdigest()

    request.session['tinymol_captcha'] = "|".join([str(time_stamp()), md5code, salt])
    img.save(response, "PNG")
    return response

@ajax_view
def check_captcha(request, captcha=None):
    try:
        if captcha == None:
            captcha = request.REQUEST['captcha']

        captcha = "".join(captcha.split())
        timestamp, md5code, salt = request.session['tinymol_captcha'].split('|')

        delta = time_stamp() - float(timestamp)
        if 0 < delta and delta < settings.CAPTCHA_LIFE:
            code = "".join([captcha, salt, 'tinymol'])
            if md5code == md5(code).hexdigest():
                return {"success": True}
            else:
                return {"success": False, "detail": "INVALID_CAPTCHA"}
        else:
            return {"success": False, "detail": "CAPTCHA_EXPIRED"}

    except KeyError:
        return {"success": False, "detail": "CAPTCHA_MISSING"}

@ajax_view
def user_available(request, user=None):
    if user == None:
        user = request.REQUEST['user']

    try:
        user = User.objects.get(email=user)
        return {"success":False, "detail":"USER_EXISTED"}
    except User.DoesNotExist:
        return {"success":True}



@csrf_exempt
@ajax_view
def load_table(request):
    result = {}
    for file in request.FILES.values():
        
        ext = extname(file.name).lower()
        if ext == '.xls' or ext == '.xlsx':
            records = result[file.name] = []
            workbook = open_workbook(file_contents=file.read())
            sheet = workbook.sheet_by_index(0)
            if sheet:
                nrows = sheet.nrows
                for i in range(nrows):
                    records.append(sheet.row_values(i))
        elif ext == '.csv':
            content = file.read().decode('CP437')
            result[file.name] = [row.split(',') for row in StringIO(content)]
        elif ext == '.txt':
            raise NotImplementedError()
        else:
            raise RuntimeError("Unknown File Format")
        
    return result

def render_file(request, path, basic_path):
    context = {'user':request.user}
    return render(request, basic_path % path, context)

