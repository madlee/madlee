from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

'''
Created on Apr 16, 2013

@author: lifej
'''

from os import makedirs as make_dirs, listdir as list_dir
from os.path import join as join_path, split as split_path, isdir as is_dir, isfile as is_file
from json import dump as dump_json, dumps as dump_json_string
from json import load as load_json, loads as load_json_string
from datetime import datetime as DateTime, date as Date, timedelta as TimeDelta
import traceback
import logging
import sys


def extname(filename):
    _, filename = split_path(filename)
    pos = filename.rfind('.')
    if pos > 0:
        return filename[pos:]
    else:
        return ''


class Timer(object):
    def __init__(self):
        self.reset()
    
    def __str__(self):
        return str(self.delta)

    @property
    def delta(self):
        return DateTime.now() - self.__start

    def reset(self):
        self.__start = DateTime.now()


ONE_DAY = TimeDelta(1)


def add_logging_argument(parser):
    LOGGING_LEVELS ='DEBUG INFO WARNING ERROR CRITICAL'.split()
    parser.add_argument('--logging-filename', help='set the name of log file', default=None)
    parser.add_argument('--logging-level', help='set the level of logging', default='', choices=LOGGING_LEVELS)
    parser.add_argument('--logging-format', help='set the format of logging', default='%(levelname)s: [%(asctime)s][%(name)s] %(message)s')
    parser.add_argument('--logging-datefmt', help='set the date/time format of logging', default='%Y/%m/%d %I:%M:%S %p')
    parser.add_argument('--logging-filemode', help='set the open mode of logfile', default='w')

def parse_logging_pargument(args):
    pars = {}
    for k, v in vars(args).iteritems():
        if k == 'logging_level' and v != '':
            pars['level'] = getattr(logging, v.upper())
        elif k.startswith('logging_'): 
            pars[k[8:]] = v

    if pars['level'] == '':
        try:
            if args.debug:
                pars['level'] = logging.DEBUG
            else:
                pars['level'] = logging.INFO
        except AttributeError:
            pars['level'] = logging.INFO

    logging.basicConfig(**pars)


def base_n(i, fillzero = 0, base="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"):
    assert i >= 0

    n = len(base)
    result = []
    while i > 0:
        result.append(base[i % n])
        i //= n

    result.reverse()
    
    if fillzero:
        n = fillzero - len(result)
        if n > 0:
            result = ['0'] * n + result
    
    if not result:
        result = ['0']

    return ''.join(result)

def now():
    return DateTime.now()

def utc_now():
    return DateTime.utcnow()

def log_error(logger):
    exc_type, exc_value, exc_traceback = sys.exc_info()
    logger.error("%s\n%s", exc_value, "".join(traceback.format_tb(exc_traceback)))
