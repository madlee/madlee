
'''
Created on Jul 25, 2014

@author: lifej
'''

from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from json import JSONEncoder as BasicJsonEncoder
from rest_framework import serializers
from django.contrib.auth.models import User, Group
from madlee.tools import DateTime, Date

class UserSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name')
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 
            'is_staff', 'is_active', 'is_superuser', 
            'date_joined', 'last_login', 'full_name')

class SimpleUserSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name')
    class Meta:
        model = User
        fields = ('id', 'full_name')

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name',)


from django.db.models import Model
from django.core.serializers import get_serializer

PythonSerializer = get_serializer('python')

def uniform_object(obj):
    if isinstance(obj, str):
        return obj
    elif isinstance(obj, DateTime):
        return str(obj)
    elif isinstance(obj, Date):
        return obj.strftime('%Y-%m-%d')
    elif isinstance(obj, Model):
        serializer = PythonSerializer()
        obj = serializer.serialize([obj], use_natural_keys=True)[0]
        id = obj['pk']
        obj = obj['fields']
        obj['id'] = id


    try:
        result = {}
        for k, v in obj.iteritems():
            result[k] = uniform_object(v)
        return result
    except AttributeError:
        try:
            result = []
            for v in obj:
                result.append(uniform_object(v))
            return result
        except TypeError:
            return obj

class JsonEncoder(BasicJsonEncoder):
    def default(self, obj):
        if isinstance(obj, Model):
            serializer = PythonSerializer()
            obj = serializer.serialize([obj], use_natural_keys=True)[0]
            return dump_json_string(obj, cls=JsonEncoder)
        elif isinstance(obj, DateTime):
            return dump_json_string(str(obj))
        return super(JsonEncoder, self).default(obj)
